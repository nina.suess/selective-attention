function [thresh] = SCRIPT_recompute_threshold(subject_id,sensmod,target_perf)
% script to recompute the stimulus intensity
% CHANGE THRESHOLD

cfg = parts.init.prepare_cfg;
filename2load = fullfile(cfg.data_path, sprintf('SC_%s_%s.mat',sensmod,subject_id));

%% Load and change Threshold

load(filename2load); 
mu = sc.muPhi; 

in.G0   = 1; % is the factor by which the sigmoid function is scaled (1 or 0.5)
in.S0   = 0; % is the intercept (0 or 0.5)
in.beta = 1;
in.INV  = 1;
[thresh,d1,d2] = sigm(target_perf,in,mu(:,end));% compute threshold value;

% substitute threshold value
if isfield(sc,'history')
	sc.history.perf(end+1) = sc.target_perf;
	sc.history.threshold(end+1) = sc.threshold;
else
	sc.history.perf(1) = 0.75;
	sc.history.threshold(1) = sc.threshold;
end
sc.threshold = thresh; 
sc.target_perf = target_perf;

%% save new value

save(filename2load, 'thresh','sc','cfgsc'); 

