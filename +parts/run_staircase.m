function [cfg] = run_staircase(subject_id, visual_staircase)
%RUN_STAIRCASE Summary of this function goes here
%   Detailed explanation goes here

ptb_config = parts.init.config_ptb;
ptb = parts.init.init_ptb(ptb_config);
parts.init.prepare_subject(subject_id);
cfg = parts.init.prepare_cfg;

ready_text = th_ptb.stimuli.visual.Text('Ready?');

ptb.draw(ready_text);
ptb.flip();
KbWait();

cfgsc               = [];
cfgsc.visgridu      = [0 96];
cfgsc.visgridu_step = 1;
cfgsc.audgridu      = [440 500];
cfgsc.audgridu_step = 1;

%change path in lab

if visual_staircase == true
  
  fun_run_trial = @parts.run_trial_vis;
  gridu         = cfgsc.visgridu(1):cfgsc.visgridu_step:cfgsc.visgridu(2); %- how to implement the colors?
  cfgsc.priorsmuPhi = [log(5/max(gridu));mean(gridu)];
  filename2saved = fullfile(cfg.data_path, sprintf('SC_vis_%s.mat', subject_id));
  
  
elseif visual_staircase == false
  
  fun_run_trial = @parts.run_trial_aud;
  gridu         = cfgsc.audgridu(1):cfgsc.audgridu_step:cfgsc.audgridu(2); % min 100
  cfgsc.priorsmuPhi = [log(40/max(gridu));mean(gridu)];
  filename2saved = fullfile(cfg.data_path, sprintf('SC_aud_%s.mat', subject_id));
  
end

nbtrialmax          = 40;
target_performance  = 0.75;
flagplot            = 0;
[thresh, sc]   = ADO_staircase(cfgsc,gridu,nbtrialmax,fun_run_trial,target_performance,flagplot); %% DO STAIRCASE

% save
%-----
save(filename2saved,'thresh','sc', 'cfgsc');

sca;

end

