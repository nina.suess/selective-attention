function [response] = run_trial_aud(cfgsc, thresh)
%RUN_TRIAL_AUD Summary of this function goes here
%   Detailed explanation goes here

% [cfg,correct] = fun_run_trial(cfg);

ptb = th_ptb.PTB.get_instance;

%% cfg ausgliedern
cfg = parts.init.prepare_cfg(6);
cfg.visual_staircase = false;

% %% load subject_data
% load(fullfile(cfg.data_path, subject_id));

%% stimuli
cfg.freq_target = thresh; % what is the current AUD stim
stim = parts.init.prepare_stims(cfg);

%% audio stimuli laden
target_trial = find(cfg.audio_sequence==2); % where is the target

fprintf('Number of target trials: %d\n', length(target_trial));

parts.audio_loop(cfg, stim);

ptb.schedule_audio;
ptb.schedule_trigger;

ptb.play_on_flip;

%% random visual stimuli anzeigen

nTrials_vis = cfg.nTrials_visual; %555 ms

%10 colours that are random between [0 0 0] and [150 150 150]

pixel_min = cfgsc.visgridu(1);
pixel_max = cfgsc.visgridu(2);

random_color = round(pixel_min+rand(1,nTrials_vis)*(pixel_max-pixel_min));
timestamp = GetSecs;
first_timestamp = [];
response_times = [];
%response = [];

my_circle = parts.init.Circle(200, 200, 0);

for i = 1:nTrials_vis
    my_circle = parts.init.Circle(200, 200,random_color(i));
    ptb.draw(my_circle);
    timestamp = ptb.flip(timestamp + cfg.ISI_visual);
    ptb.flip(timestamp + cfg.on_screen_time);
    
    if i == 1
        first_timestamp = timestamp;
    end %if
    
    %von jedem trial wait for keys
    [~, tmp_time] = ptb.wait_for_keys('target', GetSecs+0.625);
    if ~isempty(tmp_time)
        response_times(end+1) = tmp_time;
    end %if
end

%% see if the button was pressed at the correct moment...
if isempty(response_times)
    response = false;
    return;
end %if

if length(response_times) > 1
    response = -1;
    return;
end %if

button_pressed_after_onset = response_times - first_timestamp;

target_onsets = (target_trial - 1) .* cfg.ISI_audio;
button_pressed_interval = [target_onsets target_onsets + cfg.ISI_audio];

if button_pressed_after_onset >= button_pressed_interval(1) & button_pressed_after_onset <= button_pressed_interval(2)
    response = true;
else
    response = false;
end %if
end

