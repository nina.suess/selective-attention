function timing = visual_loop(cfg, stim)
%VISUAL_LOOP Summary of this function goes here
%   Detailed explanation goes here

ptb = th_ptb.PTB.get_instance;

%% for loop to create 10 visual stimuli with 1 target

%getting the timing of the flip
timestamp = GetSecs;
timing = zeros(1,cfg.nTrials_visual);

% looking at correct rate visual

% having a look at the INDEX (an welcher stelle steht das target im vektor?
% zB target an stelle 3, 8 ...in target_trial speichern
% target_trial = [3 8 9 24 ...]

%audio stuff

for i = 1:cfg.nTrials_visual
    this_condition_visual = cfg.all_conditions_visual{cfg.visual_sequence(i)};
    %here we draw the circle in the center
    ptb.draw(stim.circle.(this_condition_visual));
    %here we draw the rectangle in the right corner
    ptb.draw(stim.rect.(this_condition_visual));
    %this is about the timing
    timestamp = ptb.flip(timestamp + cfg.ISI_visual);
    ptb.flip(timestamp + cfg.on_screen_time);
    timing(i) = timestamp;
    
end %for

 %%
% % comparing response vector and visual sequence vector
% % resp2target - vergleicht response und target trial, i.e. sieht nach, ob
% % an stelle 3 oder 8 im target_trial 0 oder 1 steht und macht einen neuen
% % vektor mit 0 oder 1
% resp2target_vis = response_vis(target_trial_vis);
% 
% % die l�nge des vektors mit 1 (anzahl der response) durch die gesamte l�nge
% % des vektors mit den targets (anzahl der targets ~ 40)
% corr_det_vis = (length(find(resp2target_vis == 1))/length(target_trial_vis))*100;
% 
% disp(['Detection rate for visual = ', num2str(corr_det_vis), '%']);
% 
% resp2target_aud = response_aud(target_trial_aud);
% corr_det_aud = (length(find(resp2target_aud == 1))/length(target_trial_aud))*100;
% 
% disp(['Detection rate for audio = ', num2str(corr_det_aud), '%']);

end

