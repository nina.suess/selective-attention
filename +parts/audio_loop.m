function audio_loop(cfg, stim)
%AUDIO_LOOP Summary of this function goes here
%   Detailed explanation goes here

ptb = th_ptb.PTB.get_instance;

%% for loop for the audio sounds

for i = 1:cfg.nTrials_audio
  this_condition_audio = cfg.all_conditions_audio{cfg.audio_sequence(i)};
  ptb.prepare_audio(stim.sin_sound.(this_condition_audio), (i-1) * cfg.ISI_audio, i > 1);
  ptb.prepare_trigger(cfg.triggers_audio.(this_condition_audio), (i-1) * cfg.ISI_audio, i > 1);
  
end

end

