function [response] = run_trial_vis(cfgsc,thresh)
%FUN_RUN_TRIAL_VIS Summary of this function goes here
%   Detailed explanation goes here

ptb = th_ptb.PTB.get_instance;

%% cfg ausgliedern
cfg = parts.init.prepare_cfg(6);

cfg.visual_stairase = true;

% %% load subject_data
% load(fullfile(cfg.data_path, subject_id));

%% stimuli
fprintf('threshold: %d\n', thresh);
cfg.circle.target.color = thresh;
stim = parts.init.prepare_stims(cfg);

%% random audio stimuli
% preparing stimuli that are between 440 and 600 Hz, randomize them and
% play them

% 9 (=5,625 sec) because visual staircase (10 stimuli) takes 5,55 seconds

nTrials_audio = cfg.nTrials_audio;

sound_min = cfgsc.audgridu(1);
sound_max = cfgsc.audgridu(2);

%random selection of the deviant (9 standard, 1 random deviant)

random_sound = round(sound_min+rand(1,nTrials_audio)*(sound_max-sound_min), -1);

for i = 1:nTrials_audio
    this_sound = th_ptb.stimuli.auditory.Sine(random_sound(i), cfg.sound_duration);
    this_sound.rms = cfg.sound_rms;
    this_sound.fadeinout(0.005);
    ptb.prepare_audio(this_sound, (i-1) * cfg.ISI_audio, i > 1);
    
end %for

ptb.schedule_audio;

ptb.play_on_flip;

%% visual stimuli
timestamp = GetSecs;
nTrials_visual = cfg.nTrials_visual;
response = [];
target_trial = find(cfg.visual_sequence==2);
for i = 1:nTrials_visual
    
    this_condition_visual = cfg.all_conditions_visual{cfg.visual_sequence(i)};
    %here we draw the circle in the center
    ptb.draw(stim.circle.(this_condition_visual));
    %here we draw the rectangle in the right corner
    ptb.draw(stim.rect.(this_condition_visual));
    %this is about the timing
    timestamp = ptb.flip(timestamp + cfg.ISI_visual);
    ptb.flip(timestamp + cfg.on_screen_time);
    
    tmp = ptb.wait_for_keys({'target'}, GetSecs+0.555);
    
    response_was_correct = false;
    if ~isempty(tmp)
        fprintf('Got response!\n');
        if target_trial == i
            response_was_correct = true;
        end %if
        
        if isempty(response)
            response = response_was_correct;
        elseif ~response & ~response_was_correct
            response = false;
%         else
%             response = -1;
        end %if
    end %if    
end %for

if isempty(response)
    response = false;
end %if



end

