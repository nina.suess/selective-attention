classdef Rectangle < th_ptb.stimuli.visual.Base
  %RECTANGLE Summary of this class goes here
  %   Detailed explanation goes here
  
  properties (Access=protected)
    color;
  end %properties
  
  methods
    function obj = Rectangle(width, height, color)
      obj@th_ptb.stimuli.visual.Base();
      
      ptb = th_ptb.PTB.get_instance;
      
      obj.destination_rect(1) = width;
      obj.destination_rect(4) = height;
      obj.color = color;
      
      %rectangle in the left upper corner 
      %obj.destination_rect = [0 0 obj.width obj.height];
      
      obj.destination_rect = [1920-95-width 95 1920-95 95+obj.height];
      
    end %function
    
    function on_draw(obj, ptb)
      ptb.screen('FillRect', obj.color, obj.destination_rect);
    end %function
  end
  
end

