function prepare_subject(subject_id)
%PREPARE_SUBJECT Summary of this function goes here
%   Detailed explanation goes here

cfg = parts.init.prepare_cfg;
if ~exist(cfg.data_path)
  mkdir(cfg.data_path);
end %if

if exist(fullfile(cfg.data_path, [subject_id '.mat']))
  fprintf('The Subject is already prepared. You can go on\n');
  return;
end %if

subj_data.stim = parts.init.prepare_stims(cfg);

subj_data.block_done = 0;

save(fullfile(cfg.data_path, subject_id), 'subj_data');


end

