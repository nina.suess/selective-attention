classdef Circle < th_ptb.stimuli.visual.Base
    %CIRCLE Summary : creating a circle 
    %   creating a grey circle in the centre of the screen
    
    properties
        color;
    end %properties
    
    methods
        function obj = Circle(height, width, color)
            obj@th_ptb.stimuli.visual.Base();
            ptb = th_ptb.PTB.get_instance;
            
            obj.destination_rect(3) = width;
            obj.destination_rect(4) = height;
            obj.destination_rect = CenterRect(obj.destination_rect, ptb.win_rect);
            
            obj.color = color;
        end %function
 
        function on_draw(obj, ptb)
            ptb.screen('FillOval', obj.color, obj.destination_rect);
        end %function
        
    end
    
end

