function cfg = prepare_cfg(block_duration)
%PREPARE_CFG Summary of this function goes here
%   Detailed explanation goes here

if nargin < 1
    block_duration = 4 * 60;
end %if

cfg = [];
cfg.data_path = 'data_test';

cfg.block_duration = block_duration;

cfg.condition = {};
%% more configuration regarding the experiment

%funktion aufrufen,um  hier zuzugreifen? 
%condition1 or condition2 einbauen (worauf muss VP achten)?
%text noch einbauen - genaue erkl�rung vor dem staircase

%384 audio targets
%432 visual targets

%audio configuration inkl. triggers
cfg.ISI_audio = 1/1.6;
cfg.triggers_audio.standard = 1;
cfg.triggers_audio.target = 2;

%frequencies of the tones
cfg.freq_standard = 440;
cfg.freq_target = 600;

%duration of the sound
cfg.sound_duration = 0.025;
cfg.sound_rms = 0.05;

% visual configuration
cfg.ISI_visual = 1/1.8;
cfg.on_screen_time = 0.025;

%colour and size of circle and trigger

cfg.circle.standard.color = 0;
cfg.circle.target.color = 100;

cfg.circle.height = 200;
cfg.circle.width = 200;

cfg.rect.standard.color = [0 0 0];
cfg.rect.target.color = [255 255 255];

cfg.rect.height = 70;
cfg.rect.width = 70;

%% number of Trials
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Wenn sich die Anzahl der audio trials aus der Anzahl der visual trials
% ergibt (sind gleich, kann man berechnen), dann sollte das hier auch so
% definiert werden.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
cfg.nTrials_audio = ceil(cfg.block_duration/(cfg.ISI_audio));
cfg.nTrials_visual = ceil(cfg.block_duration/(cfg.ISI_visual));


%% creating the randomization of the audio stimuli

cfg.all_conditions_audio = {'standard', 'target'};

% here we define how many trials we want in total and the probability of it
% being a target trial.
cfg.prob_audio_target = 0.10;

%calculating total number of trials
cfg.n_audio_standard = round(cfg.nTrials_audio * (1-cfg.prob_audio_target));
cfg.n_audio_target = cfg.nTrials_audio - cfg.n_audio_standard;

%creating an array
cfg.audio_sequence = [ones(1, cfg.n_audio_standard) ones(1, cfg.n_audio_target)*2];

%finally randomize it
cfg.audio_sequence = cfg.audio_sequence(randperm(cfg.nTrials_audio));

%% creating randomization of the visual stimuli

%making an array with standard and target stimuli
cfg.all_conditions_visual = {'standard', 'target'};

%number of trials and the probability of how often the target 
%stimuli should appear
cfg.prob_visual_target = 0.10;

%total number of target and trial probability
cfg.n_visual_standard = round(cfg.nTrials_visual * (1-cfg.prob_visual_target));
cfg.n_visual_target = cfg.nTrials_visual - cfg.n_visual_standard;

%creating an array with 1 for standard stimuli and 2 for target
cfg.visual_sequence = [ones(1, cfg.n_visual_standard) ones(1, cfg.n_visual_target)*2];

%now using randperm to randomize the trials
cfg.visual_sequence = cfg.visual_sequence(randperm(cfg.nTrials_visual));

end

