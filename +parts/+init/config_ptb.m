function ptb_config = config_ptb()
%UNTITLED Summary of this function goes here
%   Detailed explanation goes here

restoredefaultpath;

%% add the path to th_ptb
addpath('C:/Users/ns_selatt/Documents/MATLAB/th_ptb/th_ptb/') % change this to where th_ptb is on your system
%addpath('/home/th/git/th_ptb/');

%addpath('C:/Users/Nina/Documents/MATLAB/th_ptb/th_ptb/')

addpath(genpath('C:/Users/ns_selatt/Documents/MATLAB/VBA-toolbox-master'));
%addpath(genpath('C:/Users/Nina/Documents/MATLAB/VBA-toolbox-master'));



%% initialize the PTB
%th_ptb.init_ptb('C:/Users/Nina/Documents/MATLAB/th_ptb/Psychtoolbox-3/'); % change this to where PTB is on your system
%th_ptb.init_ptb('/home/th/git_other/Psychtoolbox-3/Psychtoolbox/');

th_ptb.init_ptb('C:/Users/ns_selatt/Documents/MATLAB/th_ptb/Psychtoolbox-3/');


%% get a configuration object
ptb_config = th_ptb.PTB_Config();

%% do the configuration
ptb_config.fullscreen = false;
ptb_config.window_scale = 0.5;
ptb_config.skip_sync_test = true;
ptb_config.hide_mouse = false;
ptb_config.datapixxresponse_config.button_mapping('target') = ptb_config.datapixxresponse_config.Green;
ptb_config.keyboardresponse_config.button_mapping('target') = KbName('space');

ptb_config.real_experiment_sbg_cdk(false);

end

