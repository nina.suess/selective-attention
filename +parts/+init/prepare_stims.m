function stim = prepare_stims(cfg)
%PREPARE_STIMS Summary of this function goes here
%   Detailed explanation goes here

%% creating the  standard sound
%freq_standard = 440;
stim.sin_sound.standard = th_ptb.stimuli.auditory.Sine(cfg.freq_standard, cfg.sound_duration);
stim.sin_sound.standard.rms = cfg.sound_rms;
stim.sin_sound.standard.fadeinout(0.005);

%% creating the target sound
%freq_target = 600;
stim.sin_sound.target = th_ptb.stimuli.auditory.Sine(cfg.freq_target, cfg.sound_duration);
stim.sin_sound.target.rms = cfg.sound_rms;
stim.sin_sound.target.fadeinout(0.005);

%% creating the window and the stimuli (circle in the center)

%here we access the class "Circle" that we defined in order to create a
%standard and a target circle. 

stim.circle.standard = parts.init.Circle(cfg.circle.height, cfg.circle.width, cfg.circle.standard.color);
stim.circle.target = parts.init.Circle(cfg.circle.height, cfg.circle.width, cfg.circle.target.color);


%% here we define the rectangle in the right upper corner (trigger)

%everytime it flips, the array should be filled with 0 (grey), 1 (white) or
% 2 (light grey)

%TO DO 

stim.rect.standard = parts.init.Rectangle(cfg.rect.height, cfg.rect.width, cfg.rect.standard.color);
stim.rect.target = parts.init.Rectangle(cfg.rect.height, cfg.rect.width, cfg.rect.target.color);
% 


end

