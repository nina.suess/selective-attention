%% selective attention ready

%% initializationy
clear all global
restoredefaultpath

%% add the path to th_ptb
%addpath('C:/Users/Nina/Documents/MATLAB/th_ptb/th_ptb/')% change this to where th_ptb is on your system
 addpath('C:/Users/ns_selatt/Documents/MATLAB/th_ptb/th_ptb/')
%addpath('/home/th/git/th_ptb/');

%% initialize the PTB
%th_ptb.init_ptb('C:/Users/Nina/Documents/MATLAB/th_ptb/Psychtoolbox-3/');
th_ptb.init_ptb('C:/Users/ns_selatt/Documents/MATLAB/th_ptb/Psychtoolbox-3/');% change this to where PTB is on your system
%th_ptb.init_ptb('/home/th/git_other/Psychtoolbox-3/Psychtoolbox/');

%% get a configuration object
ptb_cfg = th_ptb.PTB_Config();

%% do the configuration
ptb_cfg.fullscreen = false;
ptb_cfg.window_scale = 0.2;
ptb_cfg.skip_sync_test = true;
ptb_cfg.hide_mouse = false;

%% get th_ptb.PTB object
ptb = th_ptb.PTB.get_instance(ptb_cfg);

ptb_cfg.real_experiment_sbg_cdk(true);

%% more configuration regarding the experiment

%funktion aufrufen,um  hier zuzugreifen? 
%condition1 or condition2 einbauen (worauf muss VP achten)?
%text noch einbauen - genaue erkl�rung vor dem staircase

%audio configuration inkl. triggers
ISI_audio = 1/1.6;
triggers_audio.standard = 1;
triggers_audio.target = 2;

%duration of the sound
sound_duration = 0.025;
sound_rms = 0.05;

% visual configuration
ISI_visual = 1/1.8;
on_screen_time = 0.025;

%% init audio, triggers and screen

ptb.setup_audio;
ptb.setup_trigger;
ptb.setup_screen;

%% creating the  standard sound

freq = 440;
sin_sound.standard = th_ptb.stimuli.auditory.Sine(freq, sound_duration);
sin_sound.standard.rms = sound_rms;

%% creating the target sound

freq = 600;
sin_sound.target = th_ptb.stimuli.auditory.Sine(freq, sound_duration);
sin_sound.target.rms = sound_rms;

%% creating the randomization of the audio stimuli

all_conditions_audio = {'standard', 'target'};

% here we define how many trials we want in total and the probability of it
% being a target trial.
nTrials_audio = 25;
prob_audio_target = 0.10;

%calculating total number of trials
n_audio_standard = round(nTrials_audio * (1-prob_audio_target));
n_audio_target = nTrials_audio - n_audio_standard;

%creating an array
audio_sequence = [ones(1, n_audio_standard) ones(1, n_audio_target)*2];

%finally randomize it
audio_sequence = audio_sequence(randperm(nTrials_audio));

%% creating randomization of the visual stimuli

%making an array with standard and target stimuli
all_conditions_visual = {'standard', 'target'};

%number of trials and the probability of how often the target 
%stimuli should appear
nTrials_visual = 25;
prob_visual_target = 0.10;

%total number of target and trial probability
n_visual_standard = round(nTrials_visual * (1-prob_visual_target));
n_visual_target = nTrials_visual - n_visual_standard;

%creating an array with 1 for standard stimuli and 2 for target
visual_sequence = [ones(1, n_visual_standard) ones(1, n_visual_target)*2];

%now using randperm to randomize the trials
visual_sequence = visual_sequence(randperm(nTrials_visual));

%% creating the window and the stimuli (circle in the center)

%here we access the class "Circle" that we defined in order to create a
%standard and a target circle. 
my_circle.standard = parts.init.Circle(200, 200, [0 0 0]);
my_circle.target = parts.init.Circle(200, 200, [255 255 255]);

%% here we define the rectangle in the right upper corner (trigger)

%everytime it flips, the array should be filled with 0 (grey), 1 (white) or
% 2 (light grey)
rect.standard = parts.init.Rectangle(100, 100, [0 0 0]);
rect.target =  parts.init.Rectangle(100, 100, [255 255 255]);


%% for loop for the audio sounds

for i = 1:nTrials_audio
  this_condition_audio = all_conditions_audio{audio_sequence(i)};
  ptb.prepare_audio(sin_sound.(this_condition_audio), (i-1) * ISI_audio, i > 1);
  ptb.prepare_trigger(triggers_audio.(this_condition_audio), (i-1) * ISI_audio, i > 1);
end

%% prepare to let the audio run

ptb.schedule_audio;
ptb.schedule_trigger;
ptb.play_on_flip;

%% for loop to create 10 visual stimuli with 1 target

%getting the timing of the flip
timestamp = GetSecs;

for i = 1:nTrials_visual
    this_condition_visual = all_conditions_visual{visual_sequence(i)};
    %here we draw the circle in the center
    ptb.draw(my_circle.(this_condition_visual));
    %here we draw the rectangle in the right corner
    ptb.draw(rect.(this_condition_visual));

    %this is about the timing
    timestamp = ptb.flip(timestamp + ISI_visual);
    ptb.flip(timestamp + on_screen_time);
    
end %for
sca;