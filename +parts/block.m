%% selective attention ready
function block(subject_id, block_nr) 
%% init cfg
cfg = parts.init.prepare_cfg();


%% load subject_data
load(fullfile(cfg.data_path, subject_id));
thresh_audio = load(fullfile(cfg.data_path, sprintf('SC_aud_%s.mat', subject_id)), 'thresh');
thresh_visual = load(fullfile(cfg.data_path, sprintf('SC_vis_%s.mat', subject_id)), 'thresh');

cfg.freq_target = thresh_audio.thresh;
cfg.circle.target.color = thresh_visual.thresh;

%% check if block has already been done...
if block_nr <= subj_data.block_done
  error('You already did that kind of block');
end %if

block_data = {};
block_data.cfg = cfg;

%% check if the block number is too high...
if block_nr > subj_data.block_done + 1
  fprintf('\n\n\nWARNING!!! You are skipping a block!! This is very probably NOT WHAT YOU WANT!\n');
  fprintf('If you really want to continue, press the "y" button now.\n\n\n\n');
  
  [~, key_pressed] = KbWait();
  if find(key_pressed) ~= KbName('y')
    return;
  end %if
end %if

%% init ptb
ptb_config = parts.init.config_ptb;
ptb = parts.init.init_ptb(ptb_config);
parts.init.prepare_subject(subject_id);

%% checking which condition
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% im paper wurde es abwechselnd dargeboten.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

if mod(block_nr, 2) == 0 %gerade
    cfg.condition = {'audio'};
elseif mod(block_nr, 2) == 1 %ungerade
    cfg.condition = {'visual'};
end %if

%% stimuli
stim = parts.init.prepare_stims(cfg);

%% function for audio
parts.audio_loop(cfg, stim);

%% prepare to let the audio run
ptb = th_ptb.PTB.get_instance;

wait = th_ptb.stimuli.visual.Text('Warten, bis das Experiment geladen ist...');
ptb.draw(wait);
ptb.flip();

ptb.schedule_audio;
ptb.schedule_trigger;

%%
info = th_ptb.stimuli.visual.Text('Bitte konzentriere dich, \n\n das Experiment startet gleich!');
ptb.draw(info);
ptb.flip();

WaitSecs(0.2);
KbWait();

if mod(block_nr, 2) == 0 %gerade
    info_audio = th_ptb.stimuli.visual.Text('Bitte achte nur auf den Ton \n \n und dr�cke nur dann den Knopf, \n\n  wenn sich der Ton �ndert');
    ptb.draw(info_audio);
    ptb.flip();
    WaitSecs(0.2);
    KbWait();
elseif mod(block_nr, 2) == 1 %ungerade
    info_visuell = th_ptb.stimuli.visual.Text('Bitte achte nur auf den Kreis in der Mitte \n\n und dr�cke nur dann den Knopf, \n\n wenn sich die Farbe �ndert');
    ptb.draw(info_visuell);
    ptb.flip();
    WaitSecs(0.2);
    KbWait();
end %if

ptb.play_on_flip;

%% erstes speichern
save(fullfile(cfg.data_path, subject_id), 'subj_data');

%% visual loop
block_data.visual_timing = parts.visual_loop(cfg, stim);

%% save
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Das würde ich auf jeden Fall früher speichern.
% Es wäre auch zu überlegen, ob man nicht die timestamps vom visuellen
% stimulus onset in die datei speichert. Es kann immer mal sein, dass etwas
% mit der Fotodiode nicht stimmt. In dem Fall hätte man noch weitere Daten,
% aus denen man evtl. schließen kann, wann die kamen... Außerdem wäre das
% extrem hilfreich, das einfach mal in einem der Testläufe zu
% vergleichen...
% In dem Fall könnte man die subj_data Struktur einmal vor dem Abspielen
% der visual_loop speichern, die timestamps in der visual_loop in einem
% Array speichern, von dieser zurückgeben lassen und diese dann zur
% subj_data struktur hinzufügen und nochmal speichern....
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
subj_data.block_done = block_nr;
subj_data.blocks{block_nr} = block_data;

save(fullfile(cfg.data_path, subject_id), 'subj_data');


sca

end 