function run_staircase_analysis(subject_id,sensmod)
% subject_id - subject code (string)
% sensmod - 'vis' or 'aud'

addpath(genpath('C:/Users/Nina/Documents/MATLAB/VBA-toolbox-master'));
addpath(genpath('C:/Users/ns_selatt/Documents/MATLAB/VBA-toolbox-master'));

cfg = parts.init.prepare_cfg;
filename2load = fullfile(cfg.data_path, sprintf('SC_%s_%s.mat',sensmod,subject_id));
saveFolder    = cfg.data_path;

%% load data and plot staircase
load(filename2load);

hf = figure('color',[1 1 1]);
hf2 = figure('color',[1 1 1]);

ha = subplot(3,1,1,'parent',hf);
ha1 = subplot(3,1,2,'parent',hf);
ha2 = subplot(3,1,3,'parent',hf);
set(ha,'nextplot','add')
set(ha1,'nextplot','add')
set(ha2,'nextplot','add')

xlabel(ha,'trials')
ylabel(ha,'sigmoid parameters')
xlabel(ha2,'u: design control variable (stimulus contrast)')
ylabel(ha2,'design efficiency')

t = length(sc.ADO.mu);
cla(ha2)
plot(ha2,sc.ADO.gridu,sc.ADO.e)
[v,ind]= max(sc.ADO.e);
plot(ha2,sc.ADO.gridu(ind),sc.ADO.e(ind),'go')

cla(ha)
% summarize results of adaptive dewicn strategy
plotUncertainTimeSeries(sc.ADO.mu(1,1:t),sqrt(sc.ADO.va(1,1:t)),1:t,ha,1);
hl = findobj(ha,'type','line');
hp = findobj(ha,'type','Patch');
set(hl,'color','r');
set(hp,'facecolor','r');
cla(ha1)
plotUncertainTimeSeries(sc.ADO.mu(2,1:t),sqrt(sc.ADO.va(2,1:t)),1:t,ha1,1);
hl = findobj(ha1,'type','line');
hp = findobj(ha1,'type','Patch');
set(hl,'color','g');
set(hp,'facecolor','g');

figure(hf2)
displayUncertainSigmoid_gs(sc,sc.out,hf2);

saveas(hf,fullfile(saveFolder,sprintf('SC_%s_%s_fig1',sensmod,subject_id)),'png');
saveas(hf2,fullfile(saveFolder,sprintf('SC_%s_%s_fig2',sensmod,subject_id)),'png');
sc.threshold
end
