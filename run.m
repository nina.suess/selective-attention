%% clear...
clear all global
close all

%% add paths...
restoredefaultpath
addpath('C:/Users/ns_selatt/Documents/MATLAB/th_ptb/th_ptb/')
%addpath('C:/Users/Nina/Documents/MATLAB/th_ptb/th_ptb/')

%% set variables....
subject_id = 'test01';
%subject_id = '19891222GBHL';

% ptb_config = parts.init.config_ptb;
% ptb = parts.init.init_ptb(ptb_config);
% parts.init.prepare_subject(subject_id);
% cfg = parts.init.prepare_cfg;

%% run staircase vis
 
parts.run_staircase(subject_id,true); % vis

%% looking at sigmoid visual

run_staircase_analysis(subject_id,'vis')

%% run au staircase
 
parts.run_staircase(subject_id,false); % aud

%% looking at sigmoid audio

run_staircase_analysis(subject_id,'aud')

%% run blocksy
block_nr =11;

parts.block(subject_id, block_nr);
