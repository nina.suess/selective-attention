function [thresh, sc] = ADO_staircase(cfgsc,gridu,nbtrialmax,fun_run_trial,th,flagplot)
% Adaptive Design Optimization (ADO) procedure 
% to estimate perceptual sigmoid parameters (slope and inflexion point)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
% [thresh, sc] = ADO_staircase(cfg,gridu,nbtrialmax,fun_run_trial,th,flagplot)
%
% INPUTS
%-------
% cfgsc           : config structure with cfg.thresh used in your fun_run_trial
% gridu         : vector of all stimulation space (all potential stim itensity = range of your staircase)
%                 ** What is a good practice for gridu definition? **
%                 --> if mean(gridu) = the expected sigmoid's inflexion point
%                 --> if gridu's steps are small enough to allow the best sigmoid's fit precision
% 
% nbtrialmax    : maximum number of trials for the staircase
% fun_run_trial : call name for your run trial function (i.e. @run_trial.m )
% th            : perceptual threshold seeked (Proba between 0 and 1);
%                 default --> th = 0.5)
% flagplot      : 1 --> iterative feedback plot output /
%                 2 --> iterative feedback plot output + VBA results fit
%                 0 --> No plot (default)
%
% OUTPUTS
%---------
% thresh  : stimulation value related to the perceptual threshold seeked
% sc      : staircase structure with all variables saved (including cfg structure)
%
%
%%%%%%%%%%%%%%%%%%%%%%
% Gaetan - 2016/01/15 (first implementation)
%

if nargin < 5
    th = 0.5;
    flagplot = 0;
elseif nargin < 6
    flagplot = 0;
else
end

% Paramters to simulate data
%---------------------------
if isempty(fun_run_trial)
    test_noPTB = 1; % test without a real subject (simulation)
   % flagplot   = 2;
    %simphi     = [log(3/max(gridu));mean(gridu)];
    simphi     = [0.4;mean(gridu)];
    disp (' ');
    disp('ATTENTION : You are simulating data, not working for a real experiment !!!');
    disp(' ');
    disp('Press control+c to abort. Or any other key to proceed')
    pause;    
else
    simphi = [];
    test_noPTB = 0;
end

%% Some trials limits parameters
%---------------------------------
p = nbtrialmax; %  number max of trials
NBinit_trials = 4; % initialization period

%% random selection within gridu
%---------------------------------
if NBinit_trials <= 20
    vec_sel = [min(gridu) max(gridu)];% intensity of stim for init (max and min)
    init_tr = datasample(vec_sel,numel(vec_sel),'Replace',false); % random selection of this
    
elseif NBinit_trials > 20
    section = 6; % steps of gridu for random selection during initialization
    vec_sel = [min(gridu):(max(gridu)/section):max(gridu)];% intensity of stim for init
    init_tr = datasample(vec_sel,numel(vec_sel),'Replace',false); % random selection of this
    
end

%% configure simulation and VBA inversion
%----------------------------------------
dim.n_phi   = 2;
dim.n_theta = 0;
dim.n       = 0;
dim.n_t     = 1;
dim.p       = p;

g_fname                 = @g_sigm_binomial; % Copy and change this function if the y-axis of your sigmoid is not between [0 1]
options.binomial        = 1;

options.priors.muPhi    = cfgsc.priorsmuPhi;
%options.priors.muPhi    = [log(5/max(gridu));mean(gridu)]; % [log sigmoid slope ; inflexion point]
options.priors.SigmaPhi = [0.1  0; 0 1e5];
options.DisplayWin      = 0;
options.verbose         = 0;

opt = options;

posterior = options.priors;

%% prepare graphical output window
%---------------------------------
if flagplot
    hf = figure('color',[1 1 1]);
    hf2 = figure('color',[1 1 1]);
    ha = subplot(3,1,1,'parent',hf);
    ha1 = subplot(3,1,2,'parent',hf);
    ha2 = subplot(3,1,3,'parent',hf);
    set(ha,'nextplot','add')
    set(ha1,'nextplot','add')
    set(ha2,'nextplot','add')
    xlabel(ha,'trials','fontweight','b')
    ylabel(ha,{'sigmoid parameter:';'slope (log)'})
    xlabel(ha1,'trials','fontweight','b')
    ylabel(ha1,{'sigmoid parameter:'; 'inflexion point'})
    xlabel(ha2,'u: design control variable (stimulus intensity)','fontweight','b')
    ylabel(ha2,'design efficiency')
end

%% pre-allocate trial-dependent variables
%-----------------------------------------
y = zeros(p,1);
u = zeros(p,1);
eu = zeros(p,1);
mu = zeros(dim.n_phi,p);
va = zeros(dim.n_phi,p);
e  = zeros(length(gridu),1);
sx = zeros(p,1);

count = 0;

t = 1;

while t <= p
    
    %% Update prior for design efficiency derivation
    %-------------------------------------------------
    dim.p = 1;
    opt.priors = posterior;
    
    %% Find most efficient control variable (stimulus choice)
    %---------------------------------------
    
    if t <= NBinit_trials %%%%%%%%%% Initialisation
       
        % Select random sample from initialization samples 
        %--------------------------------------------------
        count = count + 1;
        if count > numel(vec_sel)
            count = 1;
            init_tr = datasample(vec_sel,numel(vec_sel),'Replace',false);
        else
        end
        u(t) = init_tr(count);
        eu(t) = VBA_designEfficiency([],g_fname,dim,opt,u(t),'parameters');
        
        
    else %%%%%%%%% Optimisation 
      
        % Use ADO to find most efficient control variable
        %-------------------------------------------------
        for i=1:length(gridu)
            e(i) = VBA_designEfficiency([],g_fname,dim,opt,gridu(i),'parameters');
        end
        
         
        OptimalChoice = 'Peaks';
        
        switch OptimalChoice
          
          case 'Max' % No noise just the max
            [maxv,ind] = max(e);
            
          case 'Peaks' % random choice between two max
              [valpks,indpks] = findpeaks(e,'SortStr','descend'); % find peaks in efficiency by descend order
              if numel(indpks) ==  0
                  [maxv,ind] = max(e);
              else
                  
                  if numel(indpks) > 1 % if there is more than one peak
                      R    = randi([1 2]); % radom selection between 1st and 2nd max peak
                  else
                      R = 1; % take the only one
                  end
                  ind  = indpks(R);
                  maxv = e(ind);
              end
            
          case 'SoftMax' % Proba choice of the efficiency landscape
            epN = (e - min(e))/(max(e)-min(e)); % normalize e
            ep  = epN/sum(epN); % make a proba where sum(e)=1 
            R   = mnrnd(1,ep);
            ind = find(R);
            maxv= e(ind);
        end
        
        u(t) = gridu(ind);
        eu(t) = maxv;
        
        if flagplot
            % display design eficiency as a function of control variable
            %------------------------------------------------------------
            cla(ha2)
            plot(ha2,gridu,e)
            plot(ha2,gridu(ind),e(ind),'ko')
            drawnow
        end
        
    end
    
    
    %% present stimulus with current setting
    %----------------------------------------
    if test_noPTB
        % sample choice according to simulated params
        %---------------------------------------------
        sx(t) = g_sigm_binomial([],simphi,u(t),[]);
        [correct] = sampleFromArbitraryP([sx(t),1-sx(t)]',[1,0]',1);
        
        if t == 10 || t == 15
            correct = 3; % simulate a no response at trial 10 and 15
        else
        end
        
    else
        thresh   = u(t);
        [correct] = fun_run_trial(cfgsc,thresh);
        
    end
    
    fprintf('correct=%d\n', correct);
    
    % Check if trial data could be inverted
    %--------------------------------------
    if correct == -1
        continue;
    end %if
    
    if correct == 3
        opt.isYout(t,1) = 1; % if 1 = do not invert when no response
        correct = 0; % no response = Miss
    else
        opt.isYout(t,1) = 0;
    end
    
    y(t)=correct;
    
    %% invert model with all inputs and choices
    %-------------------------------------------
    dim.p = t;
    [posterior,out] = VBA_NLStateSpaceModel(y(1:t),u(1:t),[],g_fname,dim,opt);
    mu(:,t) = posterior.muPhi;
    va(:,t) = diag(posterior.SigmaPhi);
    
    
    if flagplot
        % display posterior credible intervals
        %--------------------------------------
        if t > 1
            cla(ha)
            plotUncertainTimeSeries(mu(1,1:t),sqrt(va(1,1:t)),1:t,ha,1);
            hl = findobj(ha,'type','line');
            hp = findobj(ha,'type','Patch');
            set(hl,'color','r');
            set(hp,'facecolor','r');
            cla(ha1)
            plotUncertainTimeSeries(mu(2,1:t),sqrt(va(2,1:t)),1:t,ha1,1);
            hl = findobj(ha1,'type','line');
            hp = findobj(ha1,'type','Patch');
            set(hl,'color','g');
            set(hp,'facecolor','g');
            % summarize results of adaptive design strategy
            %-----------------------------------------------
            clf(hf2)
            [handles] = displayUncertainSigmoid_gs(posterior,out,hf2);
        end
    end
    
    fprintf('incrementing t\n');
    t = t + 1;

end

if test_noPTB && flagplot == 2
    % compare final estimates with simulations
    %-----------------------------------------
    displayResults(posterior,out,y,[],[],[],simphi,[],[]);
    set(handles.ha0,'nextplot','add')
    qx = g_fname([],simphi,gridu,[]);
    plot(handles.ha0,gridu,qx,'k--')
    VBA_ReDisplay(posterior,out);
    hf = figure('color',[1 1 1]);
    ha = axes('parent',hf);
    plot(ha,eu,'k','marker','.');
    ylabel(ha,'design efficiency')
    xlabel(ha,'trials')
    box(ha,'off')
    set(ha,'ygrid','on')
end


%% output
%----------
% compute threshold value for the specific threshold
%----------------------------------------------------
in.G0   = 1; % the factor by which the sigmoid function is scaled (1 or 0.5)
in.S0   = 0; % the intercept (0 or 0.5)
in.beta = 1;
in.INV  = 1;
[thresh,d1,d2] = sigm(th,in,mu(:,end));% compute threshold value for the th;

% Create outputs
%----------------
sc = posterior;
sc.ADO.options = options;
sc.ADO.simphi  = simphi;
sc.ADO.sx   = sx;

sc.ADO.mu    = mu;
sc.ADO.y     = y;
sc.ADO.perf  = th;
sc.ADO.gridu = gridu;
sc.ADO.e     = e;
sc.ADO.va    = va;

sc.out       = out;
sc.eu        = eu;
sc.cfgsc     = cfgsc;
sc.threshold = thresh;