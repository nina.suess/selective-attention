
%% correlation

clear all global
close all

%% init obob_ownft
%addpath('C:/Users/Nina/Documents/MATLAB/obob_ownft');
addpath('C:\Users\Nina\Documents\MATLAB\obob_ownft');
cfg = [];
obob_init_ft(cfg);

%% to do

data_folder = 'C:\Users\Nina\Documents\MATLAB\masterthesis_data';
folder_aud_attend = fullfile(data_folder, 'itc_attend');
folder_cluster = 'Z:\staff\nsuess\sel_att\attentional_lapses\itc_new';

mod = 'vis';

subject_id = {
'19891222GBHL';...
'19940218HIRE';...
'19960301BIWI';...
'19930507CROT';...
'19940613SLHR';...
'19800616MRGU';...
'19870319WLGL';...
'19930819CRBO';...
'19650207CRLN';...
'19701003KTML';...
'19891207USRI';...
%'19920807GEKW';...
'19961118BRSH';...
'19930630MNSU';...
'19890516MRNM';...
'19930202JHHM';...
'19920928PTWI';...
'19923001MRTO';...
'19911209IGWL';...
'19921111BEHC';...
'19971215TEHR';...
'19980208EIOL';...
};

% loading file names audio trigger
for i = 1:numel(subject_id)
data_aud{i} = fullfile(folder_aud_attend, subject_id{i});
end

for i = 1:numel(subject_id)
data_itc_vis{i} = fullfile(data_aud{i}, sprintf('%s_itc_%s.mat', char(subject_id(i)), char(mod)));
end

% loading aud data
for i = 1:numel(subject_id)
data_tmp{i} = load(data_itc_vis{i});
itc_old{i} = data_tmp{i}.itc_vis;
end

%% extracting targets and running itc analysis

for i = 1:numel(subject_id)
idx_standard{i} = find(itc_old{i}.trialinfo == 3);
end

for i = 1:numel(subject_id)
idx = reshape(idx_standard{i}, [1, numel(idx_standard{i})]);
data_tmp{i}.F(idx, :, :) = [];
data_tmp{i}.N = size(data_tmp{i}.F,1);
clear idx
end 

%% compute new itc just for targets
for i = 1:numel(subject_id)
itc{i}.powspctrm      = data_tmp{i}.F./abs(data_tmp{i}.F);         % divide by amplitude  
itc{i}.powspctrm      = sum(itc{i}.powspctrm,1);   % sum angles
itc{i}.powspctrm      = abs(itc{i}.powspctrm)/size(data_tmp{i}.F,1);   % take the absolute value and normalize
itc{i}.powspctrm      = squeeze(itc{i}.powspctrm); % remove the first singleton dimension
end

for i = 1:numel(subject_id)
%data_tmp{i}.itc_new = data_tmp{i}.itc;
data_tmp{i}.itc_new.powspctrm = itc{i}.powspctrm;
end
%% save

for i = 1:numel(subject_id)
F = data_tmp{i}.F;
N = data_tmp{i}.N;
itc = data_tmp{i}.itc;
itc_vis = data_tmp{i}.itc_vis;
itc_new = data_tmp{i}.itc_new;


out_fname = fullfile(folder_cluster, sprintf('%s_itc_%s_targets.mat', char(subject_id{i}), char(mod)));

save(out_fname, '-v7.3', 'F', 'N', 'itc', 'itc_vis', 'itc_new');
end