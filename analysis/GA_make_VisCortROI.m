restoredefaultpath
clear all
close all

cd /mnt/obob/staff/nweisz/Rubin

addpath('/mnt/obob/obob_ownft');
 
cfg = []; 
cfg.package.svs = true;
obob_init_ft(cfg);

addpath('/mnt/obob/staff/nweisz/Rubin/mfiles/')


%%

tmp=dir('/mnt/obob/staff/npeatfield/tier1_nickp/rubinvasedata/nmuller/Data/*_rvi');

for ii=1:length(tmp)
    subjfiles{ii}=tmp(ii).name;
end
clear tmp

%%
for ii=1:length(subjfiles)
  load(['/mnt/obob/staff/nweisz/Rubin/SourceClassCoeff/' subjfiles{ii}])
  
%   cfg=[];
%   cfg.baseline=[-.25 -.05];
%   cfg.baselinetype='relchange';
%   data_source=obob_svs_timelockbaseline(cfg, data_source);
%   
  cfg=[];
  cfg.baseline=[-.25 -.05];
  data_source=nw_timelockzscore(cfg, data_source);
  data_source.avg=abs(data_source.avg);
  allsource{ii}=data_source; clear data_*
  
end

%%
cfg=[];
AVG=ft_timelockgrandaverage([], allsource{:});

%%

save sourcecoeff4plot AVG

%%
plot(AVG.time,mean( abs(AVG.avg))); xlim([-.2 .7]);

%%
load standard_mri.mat
load standard_sourcemodel3d8mm.mat

%%
cfg=[];
cfg.sourcegrid=sourcemodel;
cfg.parameter='avg';
cfg.latency=[0.25 0.35];
cfg.mri=mri;
sourcecoeff=obob_svs_virtualsens2source(cfg, AVG);

%%
%sourcecoeff.avg=abs(sourcecoeff.avg);

sourcecoeff.mask=(sourcecoeff.avg > max(sourcecoeff.avg(:))*.85);
cfg=[];
cfg.funparameter='mask';
cfg.maskparameter='mask';
cfg.funcolormap='jet';
cfg.atlas='/mnt/obob/obob_ownft/external/fieldtrip/template/atlas/aal/ROI_MNI_V4.nii';

ft_sourceplot(cfg, sourcecoeff);

save source4MASKGRANGER_250_350_85perc sourcecoeff


%%

sourceROI=sourcecoeff;
sourcecoeff.mask=(sourcecoeff.avg > max(sourcecoeff.avg(:))*.85);
sourceROI.mask=bwlabeln(sourcecoeff.mask);

[x y z] = ind2sub(size(sourceROI.mask), find(sourceROI.mask == 1)); %checken welches label man will
voxinds=[x, y, z, ones(length(x),1)];

voxpos=zeros(length(x), 3);
for kk = 1:length(x)
  tmp=sourceROI.transform * voxinds(kk,:)';
  voxpos(kk,:)=tmp(1:3);
end
%% Check outcome
cfg=[];
cfg.funparameter='avg';
cfg.maskparameter='mask';
cfg.funcolormap='jet';
cfg.location=voxpos(20000,:);
%cfg.location=sourcemodel.pos(4048,:)*10;
cfg.atlas='/mnt/obob/obob_ownft/external/fieldtrip/template/atlas/aal/ROI_MNI_V4.nii';

ft_sourceplot(cfg, sourceROI);

%% voxel positions are in mm .. CHECK! ... Sourcemodel in cm
dummymodel=sourcemodel;
dummymodel.pos=dummymodel.pos(find(dummymodel.inside==1),:);

for ii=1:length(x)
  [idx(ii)]=obob_coord2voxnr(voxpos(ii,:)/10, dummymodel);
end

idx=unique(idx);

save VisCortROI idx

%% Check outcome
cfg=[];
cfg.funparameter='mask';
cfg.maskparameter='mask';
cfg.funcolormap='jet';
%cfg.location=voxpos(20000,:);
cfg.location=dummymodel.pos(idx(75),:)*10;
cfg.atlas='/mnt/obob/obob_ownft/external/fieldtrip/template/atlas/aal/ROI_MNI_V4.nii';

ft_sourceplot(cfg, sourceROI);


