%% itc cluster

clear all global
close all

%% init obob_ownft
addpath('/mnt/obob/obob_ownft/');

cfg = [];
cfg.package.hnc_condor = true;
obob_init_ft(cfg);

%% load

%where are the files
data_folder = '/mnt/obob/staff/nsuess/sel_att/preproc/';
in_folder = fullfile(data_folder, '05_itc/');
out_folder = fullfile(data_folder, '06_grandavg');


%% itc


%mehrere subjects!!!

%IMPORTANT: nur für mags!

mod = {'aud'};

%% prepare cluster jobs...
cfg = [];
cfg.mem = '40G';
cfg.cpus = 2;
cfg.owner = 'suessni';

condor_struct = obob_condor_create(cfg);

condor_struct = obob_condor_addjob_cell(condor_struct, 'itc.grand_avg', in_folder, out_folder, mod);

%% submit jobs....

obob_condor_submit(condor_struct);
