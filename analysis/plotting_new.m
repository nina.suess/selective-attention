%% plotting - single subject

clear all global
close all

%% init obob_ownft and circstat
addpath('C:/Users/Nina/Documents/MATLAB/obob_ownft');

cfg = [];
obob_init_ft(cfg);

addpath('C:\Users\Nina\Documents\MATLAB\CircStat2012a');


%where are the files
data_folder = 'C:\Users\Nina\Nextcloud\Masterthesis_Nina\analysis';
in_folder = fullfile(data_folder, 'itc_attend');

%% load

%where are the preprocessing files
data_folder = 'C:\Users\Nina\Documents\MATLAB\masterthesis_data\itc_not_attend';
%data_folder = 'F:\Uni\Psychologie\Master\4. Semester\Masterarbeit\itc_vis';

% subject_id = '19980208EIOL';
% mod = 'vis';

subject_id = '19930507CROT';
mod = 'aud';

itc_data = fullfile(data_folder, subject_id, sprintf('%s_itc_%s_vis_trigger.mat', subject_id, mod));

load(itc_data);


%% multiplot

cfg = [];
%cfg.parameter = 'powspctrm';
cfg.layout = 'neuromag306all.lay';
ft_multiplotER(cfg, itc);

%% selecting most significant sensor, pythagoras and plot

cfg = [];
cfg.frequency = 1.8;
%cfg.channel = 'MEG0243';
itc_new = ft_selectdata(cfg, itc);


%find highest itc value sensor
max(itc_new.powspctrm)
sens = itc_new.label(itc_new.powspctrm > 0.74);



find(contains(itc_vis_not_attend.label, 'MEG0243'))


% extract fourierspectrum 
cfg = [];
cfg.frequency = 1.6;
itc_angle = ft_selectdata(cfg, itc_vis_not_attend);
itc_angle.angle_osc = angle(itc_angle.fourierspctrm);


circ_plot(itc_angle.angle_osc(:,22),'hist')

%% inferential statistics

fprintf('Inferential Statistics\n\nTests for Uniformity\n')

% Rayleigh test
p_angles = circ_rtest(itc_angle.angle_osc(:,22));
fprintf('Rayleigh Test, \t\t P = %.4f \t%.4f\n',p_angles)

