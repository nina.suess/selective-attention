
clear all global
close all

%% init obob_ownft
addpath('C:/Users/Nina/Documents/MATLAB/obob_ownft');
addpath('C:\Users\Nina\Documents\MATLAB\CircStat2012a');

cfg = [];
obob_init_ft(cfg);

%% load

%where are the files
data_folder = 'C:\Users\Nina\Documents\MATLAB\masterthesis_data';
in_folder = fullfile(data_folder, 'itc_attend');
%% loading files

mod = 'vis';

subject_id = {
'19891222GBHL';...
'19940218HIRE';...
'19960301BIWI';...
'19930507CROT';...
'19940613SLHR';...
'19800616MRGU';...
'19870319WLGL';...
'19930819CRBO';...
'19650207CRLN';...
'19701003KTML';...
'19891207USRI';...
%'19920807GEKW';...
'19961118BRSH';...
'19930630MNSU';...
'19890516MRNM';...
'19930202JHHM';...
'19920928PTWI';...
'19923001MRTO';...
'19911209IGWL';...
'19921111BEHC';...
'19971215TEHR';...
'19980208EIOL';...
};

for i = 1:numel(subject_id)
clean_data{i} = fullfile(in_folder, subject_id{i});
end

%%
%loading

for i = 1:numel(subject_id)
data_itc{i} = fullfile(clean_data{i}, sprintf('%s_itc_%s.mat', char(subject_id(i)), char(mod)));
end


%% load data normal

for i = 1:numel(subject_id)
data_tmp{i} = load(data_itc{i});
data{i} = data_tmp{i}.itc;
end 

%% combine planar

for i = 1:numel(subject_id)
itc_cmb{i} =ft_combineplanar([], data{i});
end

% computing average for gradio
for i = 1:numel(subject_id)
tmp{i} = (itc_cmb{i}.powspctrm./2);
itc_cmb{i}.powspctrm = tmp{i};
end

%% finding the one with the highest itc
for i = 1:numel(subject_id)
high{i} = max(itc_cmb{i}.powspctrm(:, 3));
end 

for i = 1:numel(subject_id)
high_new{i} = max(data{i}.powspctrm(:, 3));
end 

%% save

savefile = 'vis.mat';

high_cmb = cell2mat(high);
high = cell2mat(high_new);

save(savefile, 'high_cmb', 'high');
%% grand average

cfg = [];
%cfg.inputfile = data.itc_audio;
grandAvg_cmb = ft_freqgrandaverage(cfg, itc_cmb{:});

cfg = [];
%cfg.inputfile = data.itc_audio;
grandAvg = ft_freqgrandaverage(cfg, data{:});

%%

% attend:
% audio: 0.4 bei 21 subjects, 0.386 bei 22
% visual: 0.34 bei 22 subjects

% not attend:
% audio block, but visual trigger - 0.217 bei 22 subjects
% visual block, but audio trigger - 0.35 bei 21 subjects


% itc unabh�ngig von attention? - st�rker bei weniger subjects

cfg = [];
%cfg.parameter = 'fourierspctrm';
cfg.layout = 'neuromag306cmb.lay';
%cfg.ylim = [0 0.4];
ft_multiplotER(cfg, grandAvg_cmb);

%%
cfg = [];
%cfg.parameter = 'fourierspctrm';
cfg.layout = 'neuromag306mag.lay';
ft_multiplotER(cfg, grandAvg);


%% plotting circplot for mags

for i = 1:numel(subject_id)
cfg = [];
cfg.channel = 'megmag';
cfg.frequency = 1.6;
itc_mag{i} = ft_selectdata(cfg, data{:});
end


% cfg = [];
% cfg.inputfile = itc_mag.itc_audio;
% grandAvg_new = ft_freqgrandaverage(cfg, cfg.inputfile{:});

%%
% %average of all samples
% for i = 1:numel(subject_id)
% avg_itc = avg_itc+ itc_mag{i}.F;
%     
%     i = i+1;
% end
% 

%radians
%powspctrm = itc
for i = 1:numel(subject_id)
foi{i} = data_tmp{i}.itc_audio.fourierspctrm(:,:, 3);
angle_osc{i} = angle(foi{i});
end


%% finding the one with the highest itc


% finding row and column of highest itc value 
[row, col] = find(itc_mag.powspctrm >= 0.33);

%maybe append all subjects??
circ_plot(angle_osc(:,91),'hist')
