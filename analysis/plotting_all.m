%% plotting - single subject

clear all global
close all

%% init obob_ownft and circstat
addpath('C:/Users/Nina/Documents/MATLAB/obob_ownft');

cfg = [];
obob_init_ft(cfg);

addpath('C:\Users\Nina\Documents\MATLAB\CircStat2012a');


%where are the files
data_folder = 'C:\Users\Nina\Documents\MATLAB\masterthesis_data';
in_folder = fullfile(data_folder, 'itc_attend');

%% load

%where are the preprocessing files
%data_folder = 'C:\Users\Nina\Documents\MATLAB\masterthesis_data\itc_attend';
%data_folder = 'F:\Uni\Psychologie\Master\4. Semester\Masterarbeit\itc_vis';

subject_id = {
'19891222GBHL';...
'19940218HIRE';...
'19960301BIWI';...
'19930507CROT';...
'19940613SLHR';...
'19800616MRGU';...
'19870319WLGL';...
'19930819CRBO';...
'19650207CRLN';...
'19701003KTML';...
'19891207USRI';...
%'19920807GEKW';...
'19961118BRSH';...
'19930630MNSU';...
'19890516MRNM';...
'19930202JHHM';...
'19920928PTWI';...
'19923001MRTO';...
'19911209IGWL';...
'19921111BEHC';...
'19971215TEHR';...
'19980208EIOL';...
};

mod = 'aud';


for i = 1:numel(subject_id)
data_aud{i} = fullfile(in_folder, subject_id{i});
end

for i = 1:numel(subject_id)
data_itc_aud{i} = fullfile(data_aud{i}, sprintf('%s_itc_%s.mat', char(subject_id(i)), char(mod)));
end

%% load 
for i = 1:numel(subject_id)
data_tmp{i} = load(data_itc_aud{i});
itc_attend{i} = data_tmp{i}.itc;
end
%% multiplot

cfg = [];
%cfg.parameter = 'powspctrm';
cfg.layout = 'neuromag306mag.lay';
ft_multiplotER(cfg, itc_attend{:});


%% calculating radiants and reshaping into vector - just for mags?

cfg = [];
cfg.channel = 'megmag';
itc_mag = ft_selectdata(cfg, itc_aud_attend);

%radians
%powspctrm = itc
foi = itc_mag.fourierspctrm(:, :, 3);
angle_osc = angle(foi);

%into vector
VE1 = angle_osc(:);
VE2 = reshape(angle_osc, 1, []);
VE3 = reshape(angle_osc, numel(angle_osc), 1);

%% plotting

%to find a certain sensor that is significant
find(contains(itc_mag.label, 'MEG1921'))

% finding row and column of highest itc value 
[row, col] = find(grandAvg_cmb.powspctrm >= 0.3335);

%maybe append all subjects??
circ_plot(angle_osc(:,72),'hist')


%plotting all channels
circ_plot(VE3,'hist')


%% inferential statistics

fprintf('Inferential Statistics\n\nTests for Uniformity\n')

% Rayleigh test
p_angles = circ_rtest(angle_osc(:,216));
fprintf('Rayleigh Test, \t\t P = %.2f \t%.2f\n',p_angles)

