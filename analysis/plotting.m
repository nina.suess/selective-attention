%% plotting - single subject

clear all global
close all

%% init obob_ownft and circstat
addpath('C:/Users/Nina/Documents/MATLAB/obob_ownft');

cfg = [];
obob_init_ft(cfg);

addpath('C:\Users\Nina\Documents\MATLAB\CircStat2012a');


%where are the files
data_folder = 'C:\Users\Nina\Nextcloud\Masterthesis_Nina\analysis';
in_folder = fullfile(data_folder, 'itc_not_attend');

%% load

%where are the preprocessing files
data_folder = 'C:\Users\Nina\Documents\MATLAB\masterthesis_data\itc_not_attend';
%data_folder = 'F:\Uni\Psychologie\Master\4. Semester\Masterarbeit\itc_vis';
subject_id = '19980208EIOL';
mod = 'vis';

itc_data = fullfile(data_folder, subject_id, sprintf('%s_itc_%s_aud_trigger.mat', subject_id, mod));

load(itc_data);


%% multiplot

cfg = [];
%cfg.parameter = 'powspctrm';
cfg.layout = 'neuromag306mag.lay';
ft_multiplotER(cfg, itc);


%% calculating radiants and reshaping into vector - just for mags?

cfg = [];
cfg.channel = 'megmag';
itc_mag = ft_selectdata(cfg, itc);

%radians
%powspctrm = itc
foi = itc_aud_not_attend.fourierspctrm(:, :, 3);
angle_osc = angle(foi);

%into vector
VE1 = angle_osc(:);
VE2 = reshape(angle_osc, 1, []);
VE3 = reshape(angle_osc, numel(angle_osc), 1);

%% plotting

%to find a certain sensor that is significant
find(contains(itc_mag.label, 'MEG1921'))

max(itc_mag.powspctrm)

% finding row and column of highest itc value 
[row, col] = find(itc_mag.powspctrm >= 0.30);

%maybe append all subjects??
circ_plot(angle_osc(:,92),'hist')

%plotting all channels
circ_plot(VE3,'hist')


%% inferential statistics

fprintf('Inferential Statistics\n\nTests for Uniformity\n')

% Rayleigh test
p_angles = circ_rtest(angle_osc(:,92));
fprintf('Rayleigh Test, \t\t P = %.2f \t%.2f\n',p_angles)

