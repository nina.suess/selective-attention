%% ITC statistics
 %clear all
 
clear all global
close all

%% init obob_ownft
addpath('C:/Users/Nina/Documents/MATLAB/obob_ownft');

cfg = [];
obob_init_ft(cfg);

%% to do

data_folder = 'C:\Users\Nina\Nextcloud\Masterthesis_Nina\analysis\';
folder_aud = fullfile(data_folder, 'itc_all_aud_attend');
folder_vis_trigger = fullfile(data_folder, 'itc_vis_trigger_not_attend');

mod = 'aud';

subject_id = {
'19891222GBHL';...
'19940218HIRE';...
'19960301BIWI';...
'19930507CROT';...
'19940613SLHR';...
'19800616MRGU';...
'19870319WLGL';...
'19930819CRBO';...
'19650207CRLN';...
'19701003KTML';...
'19891207USRI';...
%'19920807GEKW';...
'19961118BRSH';...
'19930630MNSU';...
%'19890516MRNM';...
'19930202JHHM';...
'19920928PTWI';...
'19923001MRTO';...
'19911209IGWL';...
%'19921111BEHC';...
%'19971215TEHR';...
'19980208EIOL';...
};

% loading file names
for i = 1:numel(subject_id)
clean_data{i} = fullfile(folder_aud, subject_id{i});
end

for i = 1:numel(subject_id)
data_itc{i} = fullfile(clean_data{i}, sprintf('%s_itc_%s.mat', char(subject_id(i)), char(mod)));
end

%% loading data into cell

for i = 1:numel(subject_id)
data_tmp{i} = load(data_itc{i});
itc_aud_attend{i} = data_tmp{i}.itc;
end

%% loading just 1.6 Hz to run statistics

for i = 1:numel(subject_id)
itc_aud{i} = [];
itc_aud{i}.label = itc_aud_attend{i}.label;
itc_aud{i}.freq = itc_aud_attend{i}.freq(3);
itc_aud{i}.grad = itc_aud_attend{i}.grad;
itc_aud{i}.dimord = itc_aud_attend{i}.dimord;
itc_aud{i}.pow = itc_aud_attend{i}.powspctrm(:, 3);
end

% load aud vis trigger into cell


%%
cfg = [];
cfg.channel     = 'MEGGRAD';
cfg.parameter = 'powspctrm';
cfg.method      = 'analytic';
cfg.statistic   = 'ft_statfun_depsamplesT';
cfg.alpha       = 0.05;
cfg.correctm    = 'no';

Nsub = numel(subject_id);

cfg.design(1,1:2*Nsub)  = [ones(1,Nsub) 2*ones(1,Nsub)];
cfg.design(2,1:2*Nsub)  = [1:Nsub 1:Nsub];
cfg.ivar                = 1; % the 1st row in cfg.design contains the independent variable
cfg.uvar                = 2; % the 2nd row in cfg.design contains the subject number
% statistics

stat = ft_freqstatistics(cfg,itc_aud{:},itc_vis_not_attend{:});
