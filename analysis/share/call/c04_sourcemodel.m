% script to call all functions (clusterjobs) necessary to calculate
% subj-specific sourcemodel / grid either for parcellation approach or for
% wholebrain(-greyMatter) approach

% (C) A. Weise

%% clear workspace
clear all global;
close all;

%% init obob_ownft...
addpath('/mnt/obob/obob_ownft');

cfg = [];
cfg.package.hnc_condor = true;

obob_init_ft(cfg);


%% define paths
addpath('/mnt/obob/staff/aweise/data/crossfrog/scripts/');

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

chooseBrainOption = {'parcel' }% , 'whole' , 'whole_grey'} ;% *********   CHOOSE WHETHER OR NOT USING PARCELLATION APPROACH   ************************** 
% preferrably use parcellation option for erf and pow analysis; less computation time;
% for mvpa approach (brain searchlight) do not choose parcellation option as this is not
% implemented and maybe makes not too much sense
% Note that the grid resolution is fixed within the cluster function (i.e. 3mm for parcellation; 7.5 mm for whole brain (grey matter)!

data_type = 'all';
subj_ids = functions.get_subject_ids(data_type);
% subj_ids = subj_ids(2:end); % subj_ids(1:end)
    

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
  
logpath = '/mnt/obob/staff/aweise/data/crossfrog/data/meg/source/log/sourcemodel/';
 
if ~exist( 'logpath', 'dir')
    mkdir(logpath)
end

% ******************************************************

%% configure the job cluster
cfg = [];
cfg.mem = '2G';        % The amount of RAM required to run one job in the
%                         jobcluster. In "T", "G", "M" or "K".
cfg.adust_mem = true;   % default option;  job that gets held
%                         because it exceeded its requested memory will be
%                         resubmitted automatically after a couple of minutes
%                         with updated memory requirements
cfg.jobsdir = logpath;

condor_struct = obob_condor_create(cfg);

%% add jobs, i.e. call function: xyc
condor_struct = obob_condor_addjob_cell(condor_struct, 'cluster_functions.s02_sourcemodel', subj_ids, chooseBrainOption);

%% submit job
obob_condor_submit(condor_struct);






