% script to call all functions (clusterjobs) necessary to calculate
% subj-specific leadfield either for parcellation approach or for
% wholebrain-greyMatter approach

% (C) A. Weise

%% clear workspace
clear all global;
close all;

%% init obob_ownft...
addpath('/mnt/obob/obob_ownft');

cfg = [];
cfg.package.hnc_condor = true;

obob_init_ft(cfg);


%% define paths
addpath('/mnt/obob/staff/aweise/data/crossfrog/scripts/');

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
sss = {false, true}; % {true false };

chooseNormalizeOption = {'yes', 'no'};  % choose 'yes' to avoid center of the head bias in case you do not statistically contrast conditions which is the case for MVPA analysis

% ****************NOTE***********************
% if you are not contrasting the activity of interest against another condition or baseline time-window, 
% then you may choose to normalize the leadfield (cfg.normalize='yes'), which will help control against
% the power bias towards the center of the head. this you need NOT to apply
% for ERFs when contrasting sta vs dev but you need to apply when looking
% at the 'raw ERFs' (i.e. not contrasted)
% however, if you DO e.g. MVPA: you need to apply it
% ****************NOTE***********************
%
%
chooseBrainOption = { 'parcel'} % , 'whole', 'whole_grey'};  % ************************   CHOOSE WHETHER OR NOT USING PARCELLATION APPROACH   ************************** 
% use parcellation option for erf and pow analysis; less computation time;
% for mvpa approach (brain searchlight) do not choose parcellation option as this is not
% implemented and maybe makes not too much sense

chooseSensors = {'all_sensors', 'meggrad'}; % ***********************   CHOOSE WHICH SENSORS TO USE FOR SOURCE SPACE: 'all_sensors' or 'meggrad'

% analyses for all subjects with good MEG data 
data_type = 'clean'; %'meg', 'all', 'clean'
subj_ids = functions.get_subject_ids(data_type); 
%  subj_ids = subj_ids(23:27); % subj_ids(1:end)

epochType = {'sound', 'target'}; % 'sound' or 'target'; 

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
  
logpath = '/mnt/obob/staff/aweise/data/crossfrog/data/meg/source/log/leadfield/';
  
if ~exist( 'logpath', 'dir')
    mkdir(logpath)
end

% ******************************************************

%% configure the job cluster
cfg = [];
cfg.mem = '4G';        % The amount of RAM required to run one job in the
%                         jobcluster. In "T", "G", "M" or "K".
cfg.adust_mem = true;   % default option;  job that gets held
%                         because it exceeded its requested memory will be
%                         resubmitted automatically after a couple of minutes
%                         with updated memory requirements
cfg.jobsdir = logpath;

condor_struct = obob_condor_create(cfg);

%% add jobs, i.e. call function: xyc
condor_struct = obob_condor_addjob_cell(condor_struct, 'cluster_functions.s01_subjLeadfield',  subj_ids, sss, chooseNormalizeOption, chooseSensors, chooseBrainOption, epochType);
                                                                                                                                                       
%% submit job
obob_condor_submit(condor_struct);






