% script to call all functions (clusterjobs) necessary to beam time series
% data into source space

% (C) A. Weise

%% clear workspace
clear all global;
close all;

%% init obob_ownft...
addpath('/mnt/obob/obob_ownft');

cfg = [];
cfg.package.hnc_condor = true;

obob_init_ft(cfg);

%% define paths
addpath('/mnt/obob/staff/aweise/data/crossfrog/scripts/');

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
createWhichFiles = 'createAllFiles'; % 'createMissingFiles'

chooseBrainOption = {'parcel'};%  { 'parcel', 'whole' , 'whole_grey'} ;% *********   CHOOSE WHETHER OR NOT USING PARCELLATION APPROACH   ************************** 
% preferrably use parcellation option for erf and pow analysis; less computation time;
% for mvpa approach (brain searchlight) do not choose parcellation option as this is not
% implemented and maybe makes not too much sense
% Note that the grid resolution is fixed within the cluster function (i.e. 3mm for parcellation; 7.5 mm for whole brain (grey matter)!

data_type = 'clean';
subj_ids = functions.get_subject_ids(data_type);
subj_ids = {'19930118imsh',  '19810918slbr'}%subj_ids(1) % subj_ids(1:end)
    
leadfieldNormOption = {'yes' , 'no'};

sss = { true, false}; % true

choose_sensors = {'all_sensors', 'meggrad'}; % 'meggrad'

epoch = {'target'}; % 'sound' or 'target'

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
  
logpath = '/mnt/obob/staff/aweise/data/crossfrog/data/meg/source/log/pow_erf/';
 
if ~exist( 'logpath', 'dir')
    mkdir(logpath)
end

% ******************************************************

%% configure the job cluster
cfg = [];
cfg.mem = '14G';        % The amount of RAM required to run one job in the
%                         jobcluster. In "T", "G", "M" or "K".
cfg.adust_mem = true;   % default option;  job that gets held
%                         because it exceeded its requested memory will be
%                         resubmitted automatically after a couple of minutes
%                         with updated memory requirements
cfg.jobsdir = logpath;

condor_struct = obob_condor_create(cfg);

%% add jobs, i.e. call function: xyc
condor_struct = obob_condor_addjob_cell(condor_struct, 'cluster_functions.s03_svs_lcmv_pow_erf', subj_ids, chooseBrainOption,createWhichFiles, leadfieldNormOption, sss, choose_sensors,  epoch);

%% submit job
obob_condor_submit(condor_struct);






