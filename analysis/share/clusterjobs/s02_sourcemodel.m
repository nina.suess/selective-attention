function s02_sourcemodel(subj_id, chooseBrainOption)

% create individual subj grid (sourcemodel) for the parcellation approach, store it to load it during the several
% processing steps in the source analysis

% script can be run locally on pc and on server without adapting paths!

plot_grid = 'no';

if ispc
    fif_base_folder = 'O:/mnt/obob/';
else
    fif_base_folder = '/mnt/obob/';
end %if

%% init obob_ownft...
addpath(fullfile(fif_base_folder,'obob_ownft'));
cfg.package.svs = true;
cfg.package.gm2 = true;
obob_init_ft (cfg);

addpath(fullfile(fif_base_folder,'staff','aweise','data','crossfrog','scripts'));
addpath(fullfile(fif_base_folder,'staff','aweise','templates'));
coregistration_path = fullfile(fif_base_folder,'staff','aweise','data','crossfrog','data','coregistration');

%% +++++++++++++++++++++   for testing only   ++++++++++++++++++++++++++
runTest = 'no'; % 'yes' or 'no'

if strcmp(runTest, 'yes')
    
    chooseBrainOption =  'whole' % 'parcel' or 'whole' or 'whole_grey'
    
    data_type = 'meg';
    subj_ids = functions.get_subject_ids(data_type);
    subj_id = subj_ids{1};        
    
end
% ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

outpath = fullfile(fif_base_folder,'staff','aweise','data','crossfrog','data','meg','source',chooseBrainOption,'sourcemodel');

if ~exist( 'outpath', 'dir')
    mkdir(outpath)
end

%% to later be able to combine the source level data of multiple subjects one now needs to define the
%  source reconstruction grid for each individual subject in such a way
%  that all these grids are already aligned in MNI space

%% load template grid
if strcmp(chooseBrainOption, 'parcel')
    
    chooseGridType = 'grid_3mm_parcel';
    load parcellations_3mm.mat ;     
    
    % template_grid: This is the template_grid you will need to morph to each subjects anatomy to then calculate the leadfields with.
    % parcel_grid: This is the grid needed by the obob_svs_virtualsens2source function.
    % layout: Use this for ft_multiplot*
    % parcel_array: This is used internally
    
elseif strcmp(chooseBrainOption, 'whole_grey')
    
    chooseGridType = 'mni_grid0.0075greyMatter';
    load mni_grid0.0075greyMatter.mat;
    
elseif strcmp(chooseBrainOption, 'whole')

    chooseGridType = 'mni_grid0.0075';
     load mni_grid0.0075.mat;
     
end

%% load subj specific mri which is already transformed in meter
load(fullfile(coregistration_path, sprintf('%s_%s.mat', 'coregister', subj_id)), 'mri_aligned', 'hdm');

%% prepare subject grid
cfg = [];
cfg.mri = mri_aligned; % individual mri
cfg.grid.warpmni = 'yes';
if strcmp(chooseBrainOption, 'parcel')
    cfg.grid.template = parcellation.template_grid;
elseif strcmp(chooseBrainOption, 'whole_grey')
    cfg.grid.template = mni_grid_greyMatter;
elseif strcmp(chooseBrainOption, 'whole')
    cfg.grid.template = mni_grid;
end
cfg.grid.nonlinear = 'yes';
cfg.grid.unit = 'm';

subject_grid = ft_prepare_sourcemodel(cfg);

if strcmp(plot_grid, 'yes')
    
    % plot both the individual headmodel and individual inside grid positions
    figure;
    ft_plot_vol(hdm,'edgecolor' ,'r');
    ft_plot_mesh(subject_grid.pos(subject_grid.inside,:), 'facecolor', 'skin',  'edgecolor', 'none'); alpha 0.4; camlight;
    %     ft_plot_mesh(ind_subject_grid.pos(ind_subject_grid.inside,:), 'facecolor', 'skin',  'edgecolor', 'none');alpha 0.4; camlight;
    
    view([-85 7]); % 3-D graph viewpoint specification.
    title([subj_id]);
    
end

%% save
save(fullfile(outpath, [subj_id '_' chooseGridType '.mat'  ]),'subject_grid', '-v7.3');








