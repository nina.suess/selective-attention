function s01_subjLeadfield(subj_id, sss, chooseNormalizeOption, chooseSensors, chooseBrainOption, epoch)
   
% (C) Annekathrin Weise

%   NOTE for calculating the leadfield it is not necessary to take care what data exactly you provide 
%   (i.e. length of data). However, it does matter whether maxfilter (sss) was applied or not!!!
%   This is because maxfiltering the data results in different number of
%   channels compared to non-maxfiltering the data - also recall that bad channels are interpolated during the maxfiltering step!!!
%   Also note: For non-maxfiltered data: The data should be cleaned and preprocessed
%   so that the final number of channels to be used is correct. This is what the function ft_prepare_leadfield
%   requires. That is, it is neccessary to input the data on which you want to perform the
%   inverse computations, since that data generally contain the gradiometer
%   information and information about the channels that should be included in
%   the forward model computation (which should not be different between
%   sss maxfiltered data or non-maxfiltered data)

%% init obob_ownft...
addpath('/mnt/obob/obob_ownft/');
cfg.package.svs = true;
cfg.package.gm2 = true; 
obob_init_ft (cfg);

addpath('/mnt/obob/staff/aweise/data/crossfrog/scripts/');
    
%% +++++++++++++++++++++   for testing only   ++++++++++++++++++++++++++
runTest = 'no'; % 'yes' or 'no'

if strcmp(runTest, 'yes')
    
    sss = false; % false or true
    chooseSensors = 'all_sensors' % 'meggrad'
    chooseBrainOption =  'parcel' % 'parcel' or 'whole' or 'whole_grey'
    chooseNormalizeOption = 'no' % 'no' or 'yes' when interested in mvpa
    
    data_type = 'all';
    subj_ids = functions.get_subject_ids(data_type);    
    subj_id = subj_ids{1}; 
    
    epoch = 'sound'  %   or 'target'  
    
end
% ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

%% define paths
inpath = '/mnt/obob/staff/aweise/data/crossfrog/data/meg/preproc/clean/'; 
inPathGrid = ['/mnt/obob/staff/aweise/data/crossfrog/data/meg/source/' chooseBrainOption '/sourcemodel/' ];
coreg_path = '/mnt/obob/staff/aweise/data/crossfrog/data/coregistration/';
outpath = ['/mnt/obob/staff/aweise/data/crossfrog/data/meg/source/' chooseBrainOption '/leadfield/norm_' chooseNormalizeOption '/' chooseSensors '/'];
  
 %% load preprocessed MEG data
 if sss
     inputfile = fullfile(inpath, [ subj_id '_data_' epoch '_clean_sss.mat']);
 else
     inputfile = fullfile(inpath, [ subj_id '_data_' epoch '_clean.mat']);
 end
 load(inputfile);
 
 if strcmp(epoch,'sound')
     data = data_sound;
 elseif strcmp(epoch,'target')
     data = data_target;
 end

%% define type of template grid
if strcmp(chooseBrainOption, 'parcel')    
    chooseGridType = 'grid_3mm_parcel';        
elseif strcmp(chooseBrainOption, 'whole_grey')    
    chooseGridType = 'mni_grid0.0075greyMatter'; 
elseif strcmp(chooseBrainOption, 'whole')
    chooseGridType = 'mni_grid0.0075';
end

%% load individual subject grid of predefined resolution
load(fullfile(inPathGrid, [subj_id '_' chooseGridType  '.mat']), 'subject_grid');

%% load subj specific headmodel / volume
load(fullfile(coreg_path, ['coregister_' subj_id]), 'hdm');

%% create leadfield
% computes the forward model for many dipole locations on a regular 2D or 3D grid
% ****************NOTE***********************
% if you are not contrasting the activity of interest against another condition or baseline time-window, 
% then you may choose to normalize the lead field (cfg.normalize='yes'), which will help control against
% the power bias towards the center of the head. this you need NOT to apply
% for ERFs as you contrast hits vs miss
% however, if you DO e.g. MVPA: you need to apply it
% ****************NOTE***********************
cfg = [];
cfg.grid = subject_grid;
cfg.vol = hdm; % subject specific headmodel /volume
if strcmp(chooseSensors, 'all_sensors')
    cfg.channel = 'MEG';
else
    cfg.channel = chooseSensors; % chooseSensors = 'meggrad' 
end

cfg.normalize = chooseNormalizeOption; % 'yes' or 'no'

subject_leadfield = ft_prepare_leadfield(cfg, data); % for generating the leadfield / forward model  
%   it is neccessary to input the data on which you want to perform the
%   inverse computations, since that data generally contain the gradiometer
%   information and information about the channels that should be included in
%   the forward model computation
  

%% create folder in which data will be stored if not existent
if ~exist(outpath, 'dir')
    mkdir(outpath)
end
if sss
    save([outpath subj_id '_' epoch '_leadfield_' chooseGridType  '_sss.mat'], 'subject_leadfield', '-v7.3');
else
 save([outpath subj_id '_' epoch '_leadfield_' chooseGridType  '.mat'], 'subject_leadfield', '-v7.3');
end


