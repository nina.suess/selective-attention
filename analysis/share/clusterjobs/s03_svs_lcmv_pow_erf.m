function s03_svs_lcmv_pow_erf(subj_id, chooseBrainOption, createWhichFiles, leadfieldNormOption, sss, choose_sensors,  epoch)

% (C) Annekathrin Weise

%% Steps to be performed in the script to project single trial time series into source space and do tf analysis:
% load preprocessed MEG data
% load individual headmodel / volume: geometrical and magnetic properties of the head
% load the subject specific dipole grid, using the grid of the standard brain (mni): location of the sources
% for each participant, a LCMV filter is estimated based on the combined conditions for a defined time interval
% project single trial time series into source space
% do time frequency analysis
% do erf (not caring about polarity of components)
% do erf (caring about polarity a la tzvetan)

%% init obob_ownft...
addpath('/mnt/obob/obob_ownft/');
cfg.package.svs = true;
cfg.package.gm2 = true;
obob_init_ft (cfg);

addpath('/mnt/obob/staff/aweise/data/crossfrog/scripts/');


%% +++++++++++++++++++++   for testing only   ++++++++++++++++++++++++++
runTest = 'no'; % 'yes' or 'no'

if strcmp(runTest, 'yes')
    
    createWhichFiles = 'createAllFiles';
    
    choose_sensors =  'meggrad'; % 'all_sensors' or  'meggrad'
    
    chooseBrainOption = 'parcel'; % 'parcel' or 'whole'  , 'whole_grey'
    
    leadfieldNormOption = 'yes'; % 'yes' or 'no'
    
    sss= false ; % false or true; note if sss = true: lamda regularization factor is set to 5% otherwise no regularization factor is used
    
    data_type = 'meg';
    subj_ids = functions.get_subject_ids(data_type);
    subj_id = '19910610ludy' %subj_ids{1};
    
    epoch = 'target'; % 'target' or 'sound'
      
    
end
% ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
outpath_pow = ['/mnt/obob/staff/aweise/data/crossfrog/data/meg/source/' chooseBrainOption '/pow/norm_' leadfieldNormOption '/' choose_sensors '/'   ];
outpath_erf= ['/mnt/obob/staff/aweise/data/crossfrog/data/meg/source/' chooseBrainOption '/erf/norm_' leadfieldNormOption '/' choose_sensors '/'   ];

inpath = '/mnt/obob/staff/aweise/data/crossfrog/data/meg/preproc/clean/';
leadfield_path = ['/mnt/obob/staff/aweise/data/crossfrog/data/meg/source/' chooseBrainOption '/leadfield/norm_' leadfieldNormOption '/' choose_sensors '/'];


%% select option which files to create
if strcmp(createWhichFiles, 'createAllFiles')
    
    disp('create all files anew');
    
elseif strcmp(createWhichFiles, 'createMissingFiles')
    
    disp('create only missing files anew');
    
    %% in case you need to run script again because cluster jobs were successfully not for all but only for some files
    %  run for each subj/condition but you want to keep files which were generated
    %  successfully (manually check date to not keep old ones based on a previous script that
    %  has been updated!)
    if sss
        checkFile = fullfile(outpath_pow, [subj_id '_' epoch '_*_sss.mat'] );
    else
        checkFile = fullfile(outpath_pow, [subj_id '_' epoch '_*_.mat'] );
    end
    if exist(checkFile)
        return % if file exist already, do not run script
    end
    
end


%% load / define template grid
if strcmp(chooseBrainOption, 'parcel')
    
    chooseGridType = 'grid_3mm_parcel';
    load parcellations_3mm.mat ;
    
    % template_grid: This is the template_grid you will need to morph to each subjects anatomy to then calculate the leadfields with.
    % parcel_grid: This is the grid needed by the obob_svs_virtualsens2source function.
    % layout: Use this for ft_multiplot*
    % parcel_array: This is used internally
    
elseif strcmp(chooseBrainOption, 'whole_grey')
    
    chooseGridType = 'mni_grid0.0075greyMatter';
    load mni_grid0.0075greyMatter.mat;
    
elseif strcmp(chooseBrainOption, 'whole')
    
    chooseGridType = 'mni_grid0.0075';
    load mni_grid0.0075.mat;
    
end

%% load preprocessed MEG data
if sss
    inputfile = fullfile(inpath, [subj_id '_data_' epoch '_clean_sss']);
else
    inputfile = fullfile(inpath, [subj_id '_data_' epoch '_clean']);
end

load(inputfile);

if strcmp(epoch,'sound')
    data = data_sound;
elseif strcmp(epoch,'target')
    data = data_target;
end

%% select smaller data set for testing only
if strcmp(runTest, 'yes')
    cfg = [];
    cfg.trials = 1:100;
    data = ft_selectdata(cfg,data);
end

%% select the data of interest
cfg = [];
if strcmp(choose_sensors,'all_sensors')
    cfg.channel = 'MEG';
else
    cfg.channel = choose_sensors;
end
if strcmp(epoch, 'target')
    cfg.latency = [-0.9 0.9]; % if it
end

data_sel = ft_selectdata(cfg, data);

%% get subject-specific leadfield (that was calculated and saved before)
if sss
    load([leadfield_path subj_id '_' epoch '_leadfield_' chooseGridType  '_sss.mat'], 'subject_leadfield');
else
    load([leadfield_path subj_id '_' epoch '_leadfield_' chooseGridType  '.mat'], 'subject_leadfield');
end

%% project single trial time series of all conditions into source space via parcel approach
% 'svs' stands for 'simple virtual sensor'; it uses a broad-band user
% specified lcmv filter to project single trial time series into source
% space
cfg = [];
cfg.grid = subject_leadfield; % forward model
if strcmp(chooseBrainOption, 'parcel')
    cfg.parcel = parcellation; % user-specified resolution (see above)
end
cfg.fixedori = 'yes'; % fixate dipoles - is the default - leads to better results according to obob users
if sss % choose lamda regulaization factor only when maxfilter has been applied; strcmp(chooseLamdaRegularizationOption,'yes')
    cfg.regfac = '5%'; % use when data are rank deficient (usually EEG data; could also hold for MEG data e.g. when low number of trials; Joachim Gross uses 7% always for MEG data)
end
if strcmp(epoch, 'target')
    cfg.latency = [-0.9 0.9]; % otherwise problems with  ft_timelockanalysis as data has variable trial lengths e.g. starting in one trial with -1 sec while in the other with -1.004 sec
else
    cfg.cov_win = [-1 1];
end
% note on covariance window: post target activity (i.e. +1sec)
% if you contrast 2 conditions it does not have to be balanced, so you can
% have it e.g. [-1 0.4] sec; make sure to have no other stuff like
% responses via hands in this interval, especially if you you have you
% are interested in somatosensory source activity from e.g. tactile NT experiment;
% in e.g. auditory activity in e.g. an auditory experiment, the spatial filter
% should take care of it - meaning it should be able to separate different source activity
% stemming from buttonpress responses vs. auditory sensory processing

cfg.parcel_avg     = 'avg_filters'; % default; This averages the spatial filters within
% each parcel and then applies them to the sensor data.

data_lcmv = obob_svs_beamtrials_lcmv(cfg, data_sel);


cfg.fixedori = 'no';
cfg.parcel_avg = 'svd_sources'; % it first calculates the sensor time-course
%                                                  of all voxels. It then runs an svd (a kind of PCA) on
%                                                  the activity within each parcel and retains all
%                                                  components it needs to explain 95% of the variance.
%                                                  These components are then averaged.

data_lcmv_keep_polarity = obob_svs_beamtrials_lcmv(cfg, data_sel);


%% do the spectral analysis for the current stimulus type - but only for epoch type 'sound'
if strcmp(epoch, 'sound')
    
    %% get relevant conditions
    conds = functions.get_conds(epoch);
    
    for iCondition = 1:length(conds)
        
        xCondition = conds{iCondition};
        
        cond_codes = functions.get_cond_codes(xCondition,epoch);
        
        %% get relevant trial indices
        % NOTE that at this stage the trials were not matched with respect to trial
        % numbers across conditions (stabefdev, dev_left, dev_right); i.e. stabdev
        % have twice as many trials (~200 compared to ~100)
        ind_rel_trials = find(ismember(data_lcmv.trialinfo(:,4),cond_codes));
        
        cfg = [];
        
        cfg.trials = ind_rel_trials; % relevant trials of the current stimuluy type; default: all
        
        cfg.method = 'mtmconvol'; % fourier transform;
        %                         % in combination with option 'hanning'
        %                         % this function implements a singletaper
        %                         % time frequency transformation!
        
        cfg.foi = 5:1:30; % frequencies of interest. starts at 4Hz and goes up to 30Hz in 1Hz steps
        
        cfg.taper = 'hanning'; %  The option 'hanning' ensures that only
        % 				          one taper will be applied thereby not introducing
        %						  an artificial frequency smoothing
        
        % ++++++++++++++++++ define the size of the window ++++++++++++
        % define the size of the window and thus also the frequency resolution
        
        cfg.t_ftimwin = 4./cfg.foi; % The size of the window: delta t = 4/f in sec..
        %                             In this case, every window is 4 cycles long.
        %                             cfg.t_ftimwin  = vector 1 x numfoi, length of time window (in seconds)
        
        % +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
        
        
        cfg.toi = -0.5 : 0.030 : 1.0; % Time bins from the beginning to the end of the respective epoch
        
        cfg.output =  'pow'; % return the power-spectra
        
        cfg.channel = 'all'; % default: 'all';
        
        cfg.keeptrials = 'no'; % default: no; ''fourier'' requires cfg.keeptrials = ''yes'' and cfg.keeptapers = ''yes'''
        %   cfg.keeptrials = 'yes' or 'no', return individual trials or average (default = 'no')
        % keep trials also relevant when doing e.g. correlation with behavior
        
        cfg.keeptapers = 'no'; % default: no; ''fourier'' requires cfg.keeptrials = ''yes'' and cfg.keeptapers = ''yes'''
        %   cfg.keeptapers = 'yes' or 'no', return individual tapers or average (default = 'no')
        
        % +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
        
        freq = ft_freqanalysis(cfg,data_lcmv);
        
        freq.cfg = []; % remove cfg field in freq
        
        %% create folder in which data will be stored if not existent
        if ~exist(outpath_pow, 'dir')
            mkdir(outpath_pow)
        end
        
        %% save individual power data for each condition
        if sss
            
            outputfile = fullfile(outpath_pow, [subj_id '_' epoch '_' xCondition  '_sss.mat'] );
            
        else
            
            outputfile = fullfile(outpath_pow, [subj_id '_' epoch '_' xCondition  '.mat'] );
        end
        save(outputfile,'freq','-v7.3');
        
        clear xCondition cond_codes ind_rel_trials freq
    end % cond loop
end % if epoch

%% select data, condition, trial inidces and epoch length for timelock analysis
conds = functions.get_conds(epoch);

for iCondition = 1:length(conds)
    
    xCondition = conds{iCondition};
    
    cond_codes = functions.get_cond_codes(xCondition,epoch);
    
    % get relevant trial indices
    % NOTE that at this stage the trials were not matched with respect to trial
    % numbers across conditions (stabefdev, dev_left, dev_right); i.e. stabdev
    % have twice as many trials (~200 compared to ~100)
    ind_rel_trials = find(ismember(data_lcmv.trialinfo(:,4),cond_codes));
    
    ind_rel_trials_keep_polarity = find(ismember(data_lcmv_keep_polarity.trialinfo(:,4),cond_codes));

    cfg = [];
    cfg.trials =  ind_rel_trials; % relevant trials of the current stimuluy type; default: all
    cfg.latency = [-0.1 0.6]; % does not make sense to
    % use a bigger window to check for target processing
    % because sounds and targets occurred on different
    % locations, so different rials need to be collapsed
    % when checking target processing
    
    data_cut = ft_selectdata(cfg, data_lcmv);
    
    cfg.trials =  ind_rel_trials_keep_polarity;
    
    data_cut_keep_polarity = ft_selectdata(cfg, data_lcmv_keep_polarity);
    
    
    %% do timelock analysis
    cfg = [];
    
    erf = ft_timelockanalysis(cfg, data_cut);
    erf.cfg = [];    
    
    erf_keep_polarity = ft_timelockanalysis(cfg, data_cut_keep_polarity);
    erf_keep_polarity.cfg = [];
    
    %% do baseline correction (baseline correction can be performed before or after averaging; see Luck et al.)
    cfg = [];
    cfg.baseline = [-0.1 0];
    cfg.baselinetype = 'relchange';
    
    erf_relchange_bsl = obob_svs_timelockbaseline(cfg, erf); % note: by default absolute values are computed before baseline normalization
    
    cfg = [];
    cfg.baseline = [-0.1 0];
    cfg.channel = 'all';    
    
    erf_keep_polarity_abs_bsl = ft_timelockbaseline(cfg, erf_keep_polarity); % note: by default absolute values are computed before baseline normalization
    
    
    %% create folder in which data will be stored if not existent
    if ~exist(outpath_erf, 'dir')
        mkdir(outpath_erf)
    end
    
    %% save individual erf data for each condition
    if sss
        outputfile = fullfile(outpath_erf, [subj_id '_' epoch '_' xCondition   '_sss.mat'] );
    else
        outputfile = fullfile(outpath_erf, [subj_id '_' epoch '_' xCondition   '.mat'] );
    end
    
    save(outputfile,'erf', 'erf_relchange_bsl', 'erf_keep_polarity','erf_keep_polarity_abs_bsl', '-v7.3');
    
    clear xCondition cond_codes ind_rel_trials ind_rel_trials_keep_polarity erf_relchange_bsl erf_keep_polarity_abs_bsl data_cut data_cut_keep_polarity
end

% % % % % % % % % %% manually load and plot gavg powspctrm
% % % % % % % % % % calculate left vs right attention contrast
% % % % % % % % % do_plot =  'no';
% % % % % % % % % if strcmp(do_plot, 'yes')
% % % % % % % % %     
% % % % % % % % %     clear all;
% % % % % % % % %     epoch = 'sound';
% % % % % % % % %     chooseBrainOption = 'parcel';
% % % % % % % % %     leadfieldNormOption = 'no';
% % % % % % % % %     choose_sensors = 'all_sensors';
% % % % % % % % %     subj_id   = '19891203igfm';
% % % % % % % % %     
% % % % % % % % %     inpath = ['/mnt/obob/staff/aweise/data/crossfrog/data/meg/source/' chooseBrainOption '/pow/norm_' leadfieldNormOption '/' choose_sensors '/'   ];
% % % % % % % % %     
% % % % % % % % %     if sss
% % % % % % % % %         left = load(fullfile(inpath, [subj_id '_' epoch '_dev_left_sss.mat'] ));
% % % % % % % % %         right = load(fullfile(inpath, [subj_id '_' epoch '_dev_right_sss.mat'] ));
% % % % % % % % %     else
% % % % % % % % %         left = load(fullfile(inpath, [subj_id '_' epoch '_dev_left.mat'] ));
% % % % % % % % %         right = load(fullfile(inpath, [subj_id '_' epoch '_dev_right.mat'] ));
% % % % % % % % %     end
% % % % % % % % %     left_vs_right = left.freq;
% % % % % % % % %     left_vs_right.powspctrm = (left.freq.powspctrm - right.freq.powspctrm) ./ (left.freq.powspctrm + right.freq.powspctrm) ;
% % % % % % % % %     
% % % % % % % % %     sta = load(fullfile(inpath, [subj_id '_' epoch '_stabdev.mat'] ));
% % % % % % % % %     
% % % % % % % % %     left_vs_sta = left.freq;
% % % % % % % % %     left_vs_sta.powspctrm = (left.freq.powspctrm - sta.freq.powspctrm) ./ (left.freq.powspctrm + sta.freq.powspctrm) ;
% % % % % % % % %     
% % % % % % % % %     right_vs_sta = right.freq;
% % % % % % % % %     right_vs_sta.powspctrm = (right.freq.powspctrm - sta.freq.powspctrm) ./ (right.freq.powspctrm + sta.freq.powspctrm) ;
% % % % % % % % %     
% % % % % % % % %     
% % % % % % % % %     load parcellations_3mm.mat; % layout for parcellation approach
% % % % % % % % %     % plot
% % % % % % % % %     cfg = [];
% % % % % % % % %     cfg.parameter = 'powspctrm'
% % % % % % % % %     cfg.interactive = 'yes';
% % % % % % % % %     cfg.layout = parcellation.layout;
% % % % % % % % %     cfg.colorbar = 'yes';
% % % % % % % % %     %     cfg.showlabels  = 'yes';
% % % % % % % % %     cfg.xlim = [0 1];
% % % % % % % % %     %     cfg.xlim = [0 0.5]
% % % % % % % % %     figure(1); ft_multiplotTFR(cfg, left_vs_right);
% % % % % % % % %     %    figure; ft_multiplotTFR(cfg, hit_vs_miss);
% % % % % % % % %     
% % % % % % % % %     figure(2); ft_multiplotTFR(cfg, left_vs_sta);
% % % % % % % % %     
% % % % % % % % %     figure(3); ft_multiplotTFR(cfg, right_vs_sta);
% % % % % % % % %     
% % % % % % % % %     
% % % % % % % % %     % interpolate parcels on a mri
% % % % % % % % %     load('standard_mri_segmented');
% % % % % % % % %     
% % % % % % % % %     cfg = [];
% % % % % % % % %     cfg.sourcegrid = parcellation.parcel_grid;
% % % % % % % % %     cfg.parameter = 'powspctrm';
% % % % % % % % %     cfg.mri = mri_seg.bss; % A brain, scull, scalp segmentation
% % % % % % % % %     cfg.latency =  [0 0.4];
% % % % % % % % %     cfg.frequency = [8 14];
% % % % % % % % %     %     data_parc_interpol = obob_svs_virtualsens2source(cfg, left_vs_right);
% % % % % % % % %     %     data_parc_interpol = obob_svs_virtualsens2source(cfg, left_vs_sta);
% % % % % % % % %     data_parc_interpol = obob_svs_virtualsens2source(cfg, right_vs_sta);
% % % % % % % % %     data_parc_interpol.coordsys = 'mni';
% % % % % % % % %     
% % % % % % % % %     % Please note that I am loading standard_mri_segmented instead of standard_mri here.
% % % % % % % % %     % I created a segmented version of both the standard mri as well as the better standard mri.
% % % % % % % % %     % Both exist in two versions:
% % % % % % % % %     % mri_seg.bss: A brain, scull, scalp segmentation
% % % % % % % % %     % mri_seg.gwc: A gray matter, white matter and csf segmentation
% % % % % % % % %     % If you feed an mri like this to obob_svs_virtualsens2source, it will provide you with a
% % % % % % % % %     % new field called brain_mask which is a mask you can use when plotting like this:
% % % % % % % % %     
% % % % % % % % %     %     load atlas_parcel333.mat
% % % % % % % % %     atlas= ft_read_atlas('/mnt/obob/obob_ownft/external/fieldtrip/template/atlas/aal/ROI_MNI_V4.nii'); % this atlas works nicely instead of parcellation atlas!
% % % % % % % % %     
% % % % % % % % %     %% do sourceplot
% % % % % % % % %     cfg = [];
% % % % % % % % %     cfg.funparameter = 'powspctrm';
% % % % % % % % %     cfg.maskparameter = 'brain_mask';
% % % % % % % % %     cfg.atlas = atlas;
% % % % % % % % %     cfg.location = 'min'; % 'max' or 'min'
% % % % % % % % %     %     cfg.queryrange = 1; % default is 3: if you set it to value 1 then you have more 'regions' without label
% % % % % % % % %     
% % % % % % % % %     ft_sourceplot(cfg, data_parc_interpol);
% % % % % % % % %     
% % % % % % % % %     %%
% % % % % % % % %     
% % % % % % % % % end
% % % % % % % % % 
% % % % % % % % % 
% % % % % % % % % %% plotting / interpolating / sanity checks for erf
% % % % % % % % % if strcmp(do_plot, 'yes')
% % % % % % % % %     
% % % % % % % % %     clear all;
% % % % % % % % %     sss = false;
% % % % % % % % %     epoch = 'sound';
% % % % % % % % %     chooseBrainOption = 'parcel';
% % % % % % % % %     leadfieldNormOption = 'yes';
% % % % % % % % %     choose_sensors = 'all_sensors';
% % % % % % % % %     subj_id   = '19891203igfm';
% % % % % % % % %     inpath_erf= ['/mnt/obob/staff/aweise/data/crossfrog/data/meg/source/' chooseBrainOption '/erf/norm_' leadfieldNormOption '/' choose_sensors '/'   ];
% % % % % % % % %     
% % % % % % % % %     if sss == true
% % % % % % % % %         left = load(fullfile(inpath_erf, [subj_id '_' epoch '_dev_left_sss.mat'] ));
% % % % % % % % %         right = load(fullfile(inpath_erf, [subj_id '_' epoch '_dev_right_sss.mat'] ));
% % % % % % % % %     else
% % % % % % % % %         left = load(fullfile(inpath_erf, [subj_id '_' epoch '_dev_left.mat'] ));
% % % % % % % % %         right = load(fullfile(inpath_erf, [subj_id '_' epoch '_dev_right.mat'] ));
% % % % % % % % %     end
% % % % % % % % %     
% % % % % % % % %     left_vs_right = left.freq;
% % % % % % % % %     left_vs_right.powspctrm = (left.freq.powspctrm - right.freq.powspctrm) ./ (left.freq.powspctrm + right.freq.powspctrm) ;
% % % % % % % % %     
% % % % % % % % %     sta = load(fullfile(inpath, [subj_id '_' epoch '_stabdev.mat'] ));
% % % % % % % % %     
% % % % % % % % %     left_vs_sta = left.freq;
% % % % % % % % %     left_vs_sta.powspctrm = (left.freq.powspctrm - sta.freq.powspctrm) ./ (left.freq.powspctrm + sta.freq.powspctrm) ;
% % % % % % % % %     
% % % % % % % % %     right_vs_sta = right.freq;
% % % % % % % % %     right_vs_sta.powspctrm = (right.freq.powspctrm - sta.freq.powspctrm) ./ (right.freq.powspctrm + sta.freq.powspctrm) ;
% % % % % % % % %     
% % % % % % % % %     
% % % % % % % % %     
% % % % % % % % %     
% % % % % % % % %     
% % % % % % % % %     
% % % % % % % % %     if strcmp(chooseParcelOption, 'yes')
% % % % % % % % %         % plot data via multiplots using the provided parcellation.layout:
% % % % % % % % %         cfg = [];
% % % % % % % % %         cfg.layout = parcellation.layout;
% % % % % % % % %         %         cfg.xlim = [-.2 .5];
% % % % % % % % %         figure; ft_multiplotER(cfg, data_tl_bl);
% % % % % % % % %     end
% % % % % % % % %     
% % % % % % % % %     % average across channels and do single plot (see sanchez et al.,
% % % % % % % % %     % submited)
% % % % % % % % %     cfg.channel = 'all'; %average across all channels
% % % % % % % % %     %     cfg.xlim = [-.2 .5];
% % % % % % % % %     figure; ft_singleplotER(cfg, data_tl_bl);
% % % % % % % % %     
% % % % % % % % %     
% % % % % % % % %     % % %     % calculate global mean field and plot data
% % % % % % % % %     % % %     cfg = [];
% % % % % % % % %     % % %
% % % % % % % % %     % % %     data_gmf = ft_globalmeanfield(cfg,data_tl_bl);
% % % % % % % % %     
% % % % % % % % %     % % %     cfg = [];
% % % % % % % % %     % % %     %     cfg.xlim = [-.2 .5];
% % % % % % % % %     % % %     figure; ft_singleplotER(cfg, data_gmf);
% % % % % % % % %     % % %
% % % % % % % % %     
% % % % % % % % %     % interpolate parcels on a mri
% % % % % % % % %     load('standard_mri_segmented');
% % % % % % % % %     
% % % % % % % % %     cfg = [];
% % % % % % % % %     if strcmp(chooseParcelOption, 'yes')
% % % % % % % % %         cfg.sourcegrid = parcellation.parcel_grid;
% % % % % % % % %     end
% % % % % % % % %     cfg.parameter = 'avg';
% % % % % % % % %     cfg.latency = [0.08 0.12]; % choosen based on where we would expect the result
% % % % % % % % %     cfg.mri = mri_seg.bss; % A brain, scull, scalp segmentation
% % % % % % % % %     
% % % % % % % % %     data_interpol = obob_svs_virtualsens2source(cfg, data_tl_bl);
% % % % % % % % %     
% % % % % % % % %     
% % % % % % % % %     % Please note that I am loading standard_mri_segmented instead of standard_mri here.
% % % % % % % % %     % I created a segmented version of both the standard mri as well as the better standard mri.
% % % % % % % % %     % Both exist in two versions:
% % % % % % % % %     % mri_seg.bss: A brain, scull, scalp segmentation
% % % % % % % % %     % mri_seg.gwc: A gray matter, white matter and csf segmentation
% % % % % % % % %     % If you feed an mri like this to obob_svs_virtualsens2source, it will provide you with a
% % % % % % % % %     % new field called brain_mask which is a mask you can use when plotting like this:
% % % % % % % % %     % load atlas_parcel333.mat
% % % % % % % % %     
% % % % % % % % %     cfg = [];
% % % % % % % % %     cfg.funparameter = 'avg';
% % % % % % % % %     cfg.maskparameter = 'brain_mask';
% % % % % % % % %     cfg.atlas = atlas;
% % % % % % % % %     
% % % % % % % % %     ft_sourceplot(cfg, data_interpol);
% % % % % % % % %     
% % % % % % % % % end
% % % % % % % % % 
% % % % % % % % % 
