
% script extracts headshape from fif file as fif file could not be read
% when working with local matlab via sftp for an unknown reason (th and aw did not find
% a solution for this problem)

% headshpape is needed for coregistering meg and headshape data (this next step works
% best locally as the bomber has some problems when it comes to using gui
% --> extremly slow!)

clear all global;
close all;

%% init obob_ownft...
addpath('/mnt/obob/obob_ownft/');
obob_init_ft;

addpath('/mnt/obob/staff/aweise/data/crossfrog/scripts/');
outpath = '/mnt/obob/staff/aweise/data/crossfrog/data/polhemus/';
mkdir(outpath);

data_type = 'meg';
subj_ids = functions.get_subject_ids(data_type); 
sss = false; % no maxfilter applied to the raw fif data

for iSubj = 1 : length(subj_ids)
    
    subj_id =  subj_ids{iSubj };  
    
    block = 1;
    
    fif_name = functions.get_fif_names( subj_id, block, sss);   
    
    headshape = ft_read_headshape(fif_name);
    headshape = ft_convert_units(headshape,'m') ;
    
    grad = ft_read_sens(fif_name, 'fileformat','neuromag_fif','senstype','meg','coordsys','head');
    grad = ft_convert_units(grad, 'm'); % use m!

    headshape_name = ['headshape_' subj_id '.mat'];
    
    save([outpath headshape_name], 'headshape', 'grad');
end