function itc_visual_foilim(subject_id, in_folder, out_folder, mod)
%ITC_vis Summary of this function goes here
%   Detailed explanation goes here


addpath('/mnt/obob/obob_ownft/');

cfg = [];

obob_init_ft(cfg);

%% loading files
clean_data = fullfile(in_folder, subject_id);

%loading
data_itc = fullfile(clean_data, sprintf('%s_allblocks_%s.mat', subject_id, mod));

load(data_itc);

%%

%targets rausnehmen
%
cfg = [];
%sollte gemeinsames timewindow von visual und vis sein

%getriggered auf vis (links von trigger)
cfg.latency = [-((1/1.8)*5)/2 ((1/1.8)*5)/2];

%getriggered auf visuell 
% vielfaches vom ITI 

%select_data for vis
data_tmp = ft_selectdata(cfg, data_all);

%select_data for visual

%contrast zwischen vis und visual 

%power berechnen?
%%

%visitory 1.6
%visual 1.8

cfg = [];
cfg.method = 'mtmfft';
cfg.taper = 'hanning';
cfg.channel = 'meg'; %für die phase angles (gradiometer schwieriger, da phasenwinkel gemittelt werden müssen zwischen 2 sensoren)
%cfg.toi    = -1:0.1:1;

% nur 1.6 hz foi?
cfg.foilim = [1 3];

%cfg.foi = 1.6;
cfg.output = 'fourier';
itc_vis = ft_freqanalysis(cfg, data_tmp);

itc_vis.grad = data_all.grad;

%%
% make a new FieldTrip-style data structure containing the ITC
% copy the descriptive fields over from the frequency decomposition

%zeit wird nicht gebraucht 
itc = [];
itc.label     = itc_vis.label;
itc.freq      = itc_vis.freq;
itc.grad      = itc_vis.grad;

%itc.time      = itc_vis.time;
itc.dimord = 'chan_freq';
%itc.dimord    = 'chan_freq_time';

%% 

F = itc_vis.fourierspctrm;   % copy the Fourier spectrum
N = size(F,1);           % number of trials

%%
% compute inter-trial phase coherence (itpc) 
itc.powspctrm      = F./abs(F);         % divide by amplitude  
itc.powspctrm      = sum(itc.powspctrm,1);   % sum angles - phsenwinkel im F
itc.powspctrm      = abs(itc.powspctrm)/N;   % take the absolute value and normalize
itc.powspctrm      = squeeze(itc.powspctrm); % remove the first singleton dimension


%itc.powspectrm = pro channel die itpc
%% winkel
%auf magnetometer schauen (gradio schwieriger) 

%angle_osc = angle(F(:));


%% save

mkdir(out_folder);
subject_outfolder = fullfile(out_folder, subject_id);
mkdir(subject_outfolder);

out_fname = fullfile(subject_outfolder, sprintf('%s_itc_%s.mat', subject_id, mod));

save(out_fname, '-v7.3', 'itc_vis', 'F', 'N', 'itc');



end

