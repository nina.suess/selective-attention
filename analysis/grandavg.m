
clear all global
close all

%% init obob_ownft
addpath('C:/Users/Nina/Documents/MATLAB/obob_ownft');

cfg = [];
obob_init_ft(cfg);

%% load

%where are the files
data_folder = 'C:\Users\Nina\Documents\MATLAB\masterthesis_data';
%data_folder = 'F:\Uni\Psychologie\Master\4. Semester\Masterarbeit';

in_folder = fullfile(data_folder, '\itc_attend');
%% loading files

mod = 'vis';

subject_id = {
'19891222GBHL';...
'19940218HIRE';...
'19960301BIWI';...
'19930507CROT';...
'19940613SLHR';...
'19800616MRGU';...
'19870319WLGL';...
'19930819CRBO';...
'19650207CRLN';...
'19701003KTML';...
'19891207USRI';...
'19920807GEKW';...
'19961118BRSH';...
'19930630MNSU';...
'19890516MRNM';...
'19930202JHHM';...
'19920928PTWI';...
'19923001MRTO';...
'19911209IGWL';...
'19921111BEHC';...
'19971215TEHR';...
'19980208EIOL';...
};
for i = 1:numel(subject_id)
clean_data{i} = fullfile(in_folder, subject_id{i});
end

%%
%loading

for i = 1:numel(subject_id)
data_itc{i} = fullfile(clean_data{i}, sprintf('%s_itc_%s.mat', char(subject_id(i)), char(mod)));
end

%% loading data

for i = 1:numel(subject_id)

data_tmp{i} = load(data_itc{i});
data{i} = data_tmp{i}.itc;

end

%% combine planar

for i = 1:numel(subject_id)
itc_cmb{i} =ft_combineplanar([], data{i});
end

for i = 1:numel(subject_id)
tmp{i} = (itc_cmb{i}.powspctrm./2);
itc_cmb{i}.powspctrm = tmp{i};
end
%% grand average

cfg = [];
%cfg.inputfile = data.itc_audio;
grandAvg_cmb = ft_freqgrandaverage(cfg, itc_cmb{:});

cfg = [];
%cfg.inputfile = data.itc_audio;
grandAvg = ft_freqgrandaverage(cfg, data{:});

%%

cfg = [];
%cfg.parameter = 'fourierspctrm';
cfg.layout = 'neuromag306cmb.lay';
ft_multiplotER(cfg, grandAvg_cmb);

%%
figure
cfg = [];
%cfg.parameter = 'fourierspctrm';
cfg.layout = 'neuromag306mag.lay';
ft_multiplotER(cfg, grandAvg);

%% save

mkdir(out_folder);
subject_outfolder = fullfile(out_folder, subject_id);
mkdir(subject_outfolder);

out_fname = fullfile(subject_outfolder, sprintf('allsubjects_itc_%s.mat', mod));

save(out_fname, '-v7.3', 'grandAvg');