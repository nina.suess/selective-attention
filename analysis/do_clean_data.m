%% clear

clear all global
close all

%% initalize

addpath('C:/Users/Nina/Documents/MATLAB/obob_ownft');
% init ft
obob_init_ft


%-----------
% adding paths

% where is the experiment stored
config  = 'G:/selective_attention';

%when was it recorded
date = {'171005';... %geht nicht, 1
  '171010';... %2
  '171011';... %3
  '171028';...%4
  '171106';...%5
  '171109';...%6
  '171118';...%7
  '171129';...%8
  '171211';...%9
  '180703';...%10
  '180717';...%11
  '180719';...%12
};

% where are the raw files
pathdataselatt = fullfile('G:/selective_attention/subject_subject', date{10});

savedir = 'C:/Users/Nina/Nextcloud/Masterthesis_Nina/analysis/preproc/'; % my own folder for saving data

%% Files name
%-------------

iblock  = 6; % number of block
isub = 27;

scode = {
%'19910711SBSU';... nicht vollständig - schlechte staircase
%'19911209IGWL';... Maxshield data should be corrected using Maxfilter prior to importing in FieldTrip.
'19891222GBHL';... %FERTIG 1
'19940218HIRE';... %audio hat viele artefakte, aber FERTIG 2
'19960301BIWI';... %FERTIG
'19930507CROT';... %FERTIG
'19940613SLHR';... %visuell passt, audio viele artefakte, aber FERTIG
'19800616MRGU';... %FERTIG
'19870319WLGL';... %audio viele artefakte, FERTIG
'19930819CRBO';... %viele artefakte (brille?), FERTIG
'19650207CRLN';... %extremer herzschlag, FERTIG
'19701003KTML';... %FERTIG
'19891207USRI';... %FERTIG
'19920807GEKW';... %FERTIG
'19961118BRSH';... %FERTIG
'19930630MNSU';... %FERTIG
'19890516MRNM';... %verwendbar? viel herumgeschaut, sub15, FERTIG
'19930202JHHM';... %FERTIG
'19920928PTWI';... %FERTIG
'19923001MRTO';... %FERTIG
'19910823SSLD';... %throw out
'19911209IGWL';... %FERTIG
'19921111BEHC';... %FERTIG
'19971215TEHR';... %FERTIG
'19980208EIOL';... %23 %FERTIG
'19960418GBSH';... %24 - 0307
'19910811SBSU';... %25 %FERTIG
'19910612CRKE';... %26 throw out? tooo much muscle artefacts FERTIG
'19961112CAHN';... %27 %FERTIG
'19891203IGFM';... %28 %FERTIG
'19950208JLKT';... %29 - 1707 FERTIG
'19960122URFO';... %30 %FERTIG
'19900515AGKT';... %31 %FERTIG
'19910823SSLD';... %32 %FERTIG
'19681012IONW';... %throw out 33
'19941011ATSM';... %34 %FERTIG
};


%subject_folder = fullfile(sprintf('%s_s%02d_b%02d.fif',scode{isub},isub,iblock)); % name specific to subject

subject_fifname = sprintf('%s_block%01d.fif', scode{isub}, iblock);

fileName = fullfile(pathdataselatt,subject_fifname);% complete file2load path

%saving different for visual and audio

Savedata = fullfile(savedir,sprintf('%02d',isub)); % save data in my own folder cause of writing rights on server
saveName = fullfile(Savedata,sprintf('artdet_selatt%02d%02d.mat',isub,iblock)); % complete file2save path

if exist(saveName,'file') % check if saveName already exist (not delete previous artefact detection by mistake)
    error('Artefact detection file already exists --> Remove/Rename previous file to start a new artefact detection');
end

% info
%total number of blocks = 10
disp('  ');
disp('*****************');
disp('Processing :');
disp(['Sub   = ' num2str(isub)]);
disp(['Block = ' num2str(iblock)]);
disp(['File  = ' fileName]);
disp('  ');


%%
%the BIO etc. sensors in the wiki are very specific to the data, so it
%cannot be read
%if there are no eog channels, do a megmag artifact detection
% 
% Read EEG
%----------
cfg = [];
cfg.dataset = fileName;
cfg.trialdef.ntrials = 1;
cfg.trialdef.triallength = Inf;
cfg = ft_definetrial(cfg);
cfg.channel = {'EOG001', 'EOG002', 'ECG003'}; % read MEG channels + all BIO channels (EOG and ECG)
dataeeg = ft_preprocessing(cfg);

% % Rename BIO sensors
% %--------------------
% sensors = {'EOG1' 'EOG2' 'ECG'};
% for i = 1:length(sensors)
%     dataeeg.label{i} = sensors{i};
% end
% 

% Read MEG
%----------
cfg = [];
cfg.dataset = fileName;
cfg.trialdef.ntrials = 1;
cfg.trialdef.triallength = Inf;
cfg = ft_definetrial(cfg);
cfg.channel = {'MEG'}; % read MEG channels + all BIO channels (EOG and ECG)
cfg.hpfilter = 'yes';
cfg.hpfreq = 1;

datameg = ft_preprocessing(cfg);


%% Apply signal space projection (SSP): reject external noise
 %-----------------------------------------------------------
 
cfg = [];
cfg.inputfile = fileName;
datameg_ssp = obob_apply_ssp(cfg, datameg);


% %append without ssp
% cfg = [];
% data_ssp = ft_appenddata(cfg,datameg,dataeeg);

% Append
%--------
cfg = [];
data_ssp = ft_appenddata(cfg,datameg_ssp,dataeeg);

labels = {'MEG0111', 'MEG0121', 'MEG0131', 'MEG0141', 'MEG0211', 'MEG0221', 'MEG0231', 'MEG0241', 'MEG0321', 'MEG0341', 'MEG0411', 'MEG0441', 'MEG1121', 'MEG1131', 'MEG1221', 'MEG1231', 'MEG1311', 'MEG1321', 'MEG1331', 'MEG1341', 'MEG1411', 'MEG1421', 'MEG1431', 'MEG1441', 'MEG1511', 'MEG1521', 'MEG1531', 'MEG1541', 'MEG1611', 'MEG1621', 'MEG1631', 'MEG1641', 'MEG1721', 'MEG1811', 'MEG1941', 'MEG2221', 'MEG2321', 'MEG2411', 'MEG2421', 'MEG2431', 'MEG2441', 'MEG2521', 'MEG2611', 'MEG2621', 'MEG2631', 'MEG2641'};


%% 1st Data visualization 
%-------------------------
% Here the idea is to just look and identify really bad sensors
%---------------------------------------------------------------
disp('  ');
disp('*****************');
disp('1st Visu...');
disp('  ');

cfg = [];
cfg.blocksize      = 30;    % show segments of 30sec
cfg.channel        = {'MEG'}; % all MEG channels together
cfg.megscale       = 1e2;
cfg.gradscale      = 0.04;  % factor to compensate different scale between GRAD and MAG
cfg.layout         = 'neuromag306mag.lay';
cfg.viewmode       = 'butterfly';
cfg.showlabel      = 'yes';
% lowpass filter (for visualization)
cfg.preproc.lpfreq   = 45;
cfg.preproc.lpfilter = 'yes';
% cfg.preproc.bsfilter = 'yes';
% cfg.preproc.bsfreq = [16.7-0.5 16.7+0.5];

ft_databrowser(cfg, data_ssp);

%% 1st Data visu (just few sensors + EOG + ECG)
%-----------------
% Here the idea is maybe to start visual definition of clear artefacted time periods
% Maybe track more temporal bad sensors
% And also check visually the impact of ECG and EOG artefacts on sensors
%------------------------------------------------------------------------
disp('  ');
disp('*****************');
disp('1st Visu (less sensors)...');
disp('  ');


cfg = [];
cfg.blocksize   = 10;    % show segments of 30sec
cfg.channel     = labels; %some MEG channels together
cfg.channel     = 'megmag'; %some MEG channels together
cfg.channelclamped = {'EOG001', 'EOG002', 'ECG003'};
% cfg.mychan         = {'EOG1'};
%cfg.mychanscale    = 1e-1;
cfg.megscale       = 1e1;
cfg.eogscale       = 1e-7;
cfg.ecgscale       = 1e-8;
cfg.gradscale      = 0.04;  % factor to compensate different scale between GRAD and MAG
cfg.layout      = 'neuromag306mag.lay';
cfg.viewmode    = 'vertical';
cfg.showlabel   = 'yes';
% lowpass filter (for visualization)
cfg.preproc.lpfreq   = 45;
cfg.preproc.lpfilter = 'yes';
cfg.preproc.hpfreq   = 1;
cfg.preproc.hpfilter = 'yes';

cfg_art = [];
cfg_art = ft_databrowser(cfg, data_ssp);

cfg_art.artfctdef; % this output cfg_art structure field will be used to gather all your upcoming artifact detection

%% Cut the data into 20 big chunks without overlap
    %--------------------------------------------------

    nbchunks = 20;% number of data chunks
    trldur   = data_ssp.time{1}(end)/nbchunks-0.001;% duration of each data chunk (-1ms: ensure to get all the chunks, but this will cut [0.001*nbchunks]sec at the end of the dataset)

    cfg = [];
    cfg.length  = trldur;
    cfg.overlap = 0;

    data_ssp_chunks = ft_redefinetrial(cfg, data_ssp);
    
 %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % Bad channels detection (use variance & kurtosis metrics)
    %--------------------------------------------------------
    % IDENTIFY LONG LASTING BAD CHANNELS AT THIS STEP!
    %---------------------------------------------------

    cfg = [];
    cfg.method      = 'summary';
    cfg.layout      = 'neuromag306mag.lay';
    cfg.channel     = 'MEG';
    cfg.gradscale   = 0.04;

    data_ssp_chunks_badchanremoved = obob_rejectvisual(cfg,data_ssp_chunks);


    % Save good channels labels & indexing
    %--------------------------------------

    allchan   = data_ssp.label;
    chan2keep = data_ssp_chunks_badchanremoved.label;

    okchan.label = [];
    okchan.index = [];
    [okchan.label,okchan.index,~] = intersect(allchan,chan2keep);

     
    % Remove bad channels = Use of okchan
    %------------------------------------------

    cfg = [];
    cfg.channel = {okchan.label{:}}; % keep only Good MEG channels
    data_ssp_chunks_okchan = ft_selectdata(cfg,data_ssp_chunks);
    
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% %% Occasional channels Jump
% %----------------------------
% disp('  ');
% disp('*****************');
% disp('CHANNELS JUMP...');
% disp('  ');
% 
% senstype = {'megmag' 'meggrad'};
% 
% for i = 1:numel(senstype)
%     
%     cfg = [];
%     % channel selection, cutoff and padding
%     cfg.artfctdef.zvalue.channel    = senstype{i};
%     cfg.artfctdef.zvalue.cutoff     = 15;
%     cfg.artfctdef.zvalue.trlpadding = 0; % Trialpadding extends the period around the trial where artifact detection is performed
%     cfg.artfctdef.zvalue.artpadding = 0.1; % Artifact padding extends the segment of data that will be marked as artifact
%     cfg.artfctdef.zvalue.fltpadding = 0; % Filter padding is only used during filtering and removed afterwards
%     
%     % algorithmic parameters
%     cfg.artfctdef.zvalue.cumulative    = 'yes';
%     cfg.artfctdef.zvalue.medianfilter  = 'yes';
%     cfg.artfctdef.zvalue.medianfiltord = 9;
%     cfg.artfctdef.zvalue.absdiff       = 'yes';
%     
%     % make the process interactive
%     cfg.artfctdef.zvalue.interactive = 'yes';
%     
%     [cfg_jump_sens, artifact_jump] = ft_artifact_zvalue(cfg, data_ssp_chunks_okchan);
%     
%     artifact_jump_senstype.(senstype{i}) = artifact_jump; % keep track of your jump artifact detection for each type of sensors
%     
% end
 %channel jumps raus, blinks drin lassen, sehr noisy daten raus, muskel
 %auch raus

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Muscular artifact (high frequency)
%--------------------------------------
disp('*****************');
disp('MUSCULAR ARTEFACT...');
disp('  ');


cfg = [];
% channel selection, cutoff and padding
cfg.artfctdef.zvalue.channel     = 'megmag';
cfg.artfctdef.zvalue.cutoff      = 15;
cfg.artfctdef.zvalue.trlpadding  = 0;
cfg.artfctdef.zvalue.fltpadding  = 0;
cfg.artfctdef.zvalue.artpadding  = 0.2;

% algorithmic parameters
cfg.artfctdef.zvalue.bpfilter    = 'yes';
cfg.artfctdef.zvalue.bpfreq      = [110 140];
cfg.artfctdef.zvalue.bpfiltord   = 9;
cfg.artfctdef.zvalue.bpfilttype  = 'but';
cfg.artfctdef.zvalue.hilbert     = 'yes';

% make the process interactive?
cfg.artfctdef.zvalue.interactive = 'yes'; 

[cfg_muscle, artifact_muscle] = ft_artifact_zvalue(cfg, data_ssp_chunks_okchan);


%%%%%%%%%%%%%%%%%%
% %% Blink on MAG
% %-------------
% disp('  ');
% disp('*****************');
% disp('BLINKS on MAG ...');
% disp('  ');
% 
% cfg = [];
% % channel selection, cutoff and padding
% cfg.artfctdef.zvalue.channel     = 'megmag'; % or one 'EOG' channel if you have one
% cfg.artfctdef.zvalue.cutoff      = 15;
% cfg.artfctdef.zvalue.trlpadding  = 0;
% cfg.artfctdef.zvalue.fltpadding  = 0;
% cfg.artfctdef.zvalue.artpadding  = 0.1;
% 
% % algorithmic parameters
% cfg.artfctdef.zvalue.bpfilter   = 'yes';
% cfg.artfctdef.zvalue.bpfilttype = 'but';
% cfg.artfctdef.zvalue.bpfreq     = [1 15];
% cfg.artfctdef.zvalue.bpfiltord  = 4;
% cfg.artfctdef.zvalue.hilbert    = 'yes';
% 
% % make the process interactive?
% cfg.artfctdef.zvalue.interactive = 'yes';
% 
% [cfg_blink, artifact_blink] = ft_artifact_zvalue(cfg, data_ssp_chunks_okchan);

% %% Blink on EOG
% %---------------
% disp('  ');
% disp('*****************');
% disp('BLINKS on EOG ...');
% disp('  ');
% 
% cfg = [];
% % channel selection, cutoff and padding
% cfg.artfctdef.zvalue.channel     = 'EOG001'; % or one 'EOG' channel if you have one
% cfg.artfctdef.zvalue.cutoff      = 2;
% cfg.artfctdef.zvalue.trlpadding  = 0;
% cfg.artfctdef.zvalue.fltpadding  = 0;
% cfg.artfctdef.zvalue.artpadding  = 0.1;
% 
% % algorithmic parameters
% cfg.artfctdef.zvalue.bpfilter   = 'yes';
% cfg.artfctdef.zvalue.bpfilttype = 'but';
% cfg.artfctdef.zvalue.bpfreq     = [1 15];
% cfg.artfctdef.zvalue.bpfiltord  = 4;
% cfg.artfctdef.zvalue.hilbert    = 'yes';
% 
% % make the process interactive?
% cfg.artfctdef.zvalue.interactive = 'yes';
% 
% [cfg_blinkEOG, artifact_blinkEOG] = ft_artifact_zvalue(cfg, data_ssp_chunks);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%
%% Keep track of previous artifact detection
%-------------------------------------------- 
% cfg_art.artfctdef.jump_MAG.artifact    = artifact_jump_senstype.megmag;
% cfg_art.artfctdef.jump_GRAD.artifact   = artifact_jump_senstype.meggrad;
cfg_art.artfctdef.muscle.artifact      = artifact_muscle;
% cfg_art.artfctdef.blink.artifact       = artifact_blink;
% cfg_art.artfctdef.blinkEOG.artifact    = artifact_blinkEOG;

%% 2nd Data visualization (MAG only)
%-----------------------------
disp('  ');
disp('*****************');
disp('Last VISU ...');
disp('  ');


cfg_art.channel     = 'megmag'; % all sensors 'MEG' or 'meggrad' or 'megmag'
%cfg_art.channel     = labels; % only MAG RL Temporal
cfg_art.channelclamped = {'EOG001', 'EOG002', 'ECG003'};
cfg_art.megscale       = 1e1;
cfg_art.eogscale       = 1e-7;
cfg_art.ecgscale       = 1e-8;
cfg_art.gradscale      = 0.04;  % factor to compensate different scale between GRAD and MAG
cfg_art.layout      = 'neuromag306mag.lay';
cfg_art.viewmode    = 'vertical';
cfg_art.showlabel   = 'yes';
% filter (for visualization)
%----------------------------
cfg_art.preproc.lpfreq   = 145;
cfg_art.preproc.lpfilter = 'yes';
cfg_art.preproc.hpfreq   = 1;
cfg_art.preproc.hpfilter = 'yes';

cfg_art = ft_databrowser(cfg_art, data_ssp_chunks_okchan);


%% Save your artifact and bad channels detection
%-----------------------------------------------
disp('  ');
disp('*****************');
disp('SAVING artefact detection ...');
disp(['File output = ' saveName]);
disp('  ');

if ~exist(saveName,'file')
    save(saveName,'cfg_art','okchan'); % save 'cfg_art' and 'okchan'
else
    [p,f,e] = fileparts(saveName);
    saveNamebis = fullfile(p,['bis_' f e]);
    warning('File already exists --> Current file will be saved under another name =');
    disp(saveNamebis);
    save(saveNamebis,'cfg_art','okchan'); % save 'cfg_art' and 'okchan'
end

disp('*****************');
disp('DONE !');