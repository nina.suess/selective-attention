%% ITC statistics
 %clear all
 
clear all global
close all

%% init obob_ownft
addpath('C:/Users/Nina/Documents/MATLAB/obob_ownft');


cfg = [];
obob_init_ft(cfg);

%% to do

%where are the files
data_folder = 'C:\Users\Nina\Documents\MATLAB\masterthesis_data';
folder_attend = fullfile(data_folder, 'itc_attend');
folder_not_attend = fullfile(data_folder, 'itc_not_attend');

%% load
mod = 'vis';

subject_id = {
'19891222GBHL';...
'19940218HIRE';...
'19960301BIWI';...
'19930507CROT';...
'19940613SLHR';...
'19800616MRGU';...
'19870319WLGL';...
'19930819CRBO';...
'19650207CRLN';...
'19701003KTML';...
'19891207USRI';...
%'19920807GEKW';...
'19961118BRSH';...
'19930630MNSU';...
'19890516MRNM';...
'19930202JHHM';...
'19920928PTWI';...
'19923001MRTO';...
'19911209IGWL';...
'19921111BEHC';...
'19971215TEHR';...
'19980208EIOL';...
};

% loading file names visual trigger
for i = 1:numel(subject_id)
data_vis{i} = fullfile(folder_attend, subject_id{i});
end

for i = 1:numel(subject_id)
data_itc_vis{i} = fullfile(data_vis{i}, sprintf('%s_itc_%s.mat', char(subject_id(i)), char(mod)));
end


%loading file names audio trigger
for i = 1:numel(subject_id)
data_not_attend{i} = fullfile(folder_not_attend, subject_id{i});
end

for i = 1:numel(subject_id)
data_not_attend{i} = fullfile(data_not_attend{i}, sprintf('%s_itc_vis_aud_trigger.mat', char(subject_id(i))));
end


%% loading data into cell

% loading vis data
for i = 1:numel(subject_id)
data_tmp{i} = load(data_itc_vis{i});
itc_vis_attend{i} = data_tmp{i}.itc;
end

clear data_tmp

% loading vis data
for i = 1:numel(subject_id)
data_tmp{i} = load(data_not_attend{i});
itc_vis_not_attend{i} = data_tmp{i}.itc;
end

clear data_tmp
%% loading just 1.8 Hz to run statistics

for i = 1:numel(subject_id)
cfg = [];
cfg.frequency = 1.8;
itc_vis{i} = ft_selectdata(cfg, itc_vis_attend{i});
end %

for i = 1:numel(subject_id)
cfg = [];
cfg.frequency = 1.6;
itc_not_attend{i} = ft_selectdata(cfg, itc_vis_not_attend{i});
end %

% for i = 1:numel(subject_id)
% % load vis trigger into cell
% itc_vis{i} = [];
% itc_vis{i}.label = itc_vis_attend{i}.label;
% itc_vis{i}.freq = itc_vis_attend{i}.freq(3);
% itc_vis{i}.grad = itc_vis_attend{i}.grad;
% itc_vis{i}.dimord = itc_vis_attend{i}.dimord;
% itc_vis{i}.powspctrm = itc_vis_attend{i}.powspctrm(:, 3);
% 
% % load vis aud trigger into cell
% itc_not_attend{i} = [];
% itc_not_attend{i}.label = itc_vis_not_attend{i}.label;
% itc_not_attend{i}.freq = itc_vis_not_attend{i}.freq(3);
% itc_not_attend{i}.grad = itc_vis_not_attend{i}.grad;
% itc_not_attend{i}.dimord = itc_vis_not_attend{i}.dimord;
% itc_not_attend{i}.powspctrm = itc_vis_not_attend{i}.powspctrm(:, 3);
% 
% end

for i = 1:numel(subject_id)
  itc_not_attend{i}.freq = itc_vis{1}.freq;
end

%%
cfg = [];
cfg.channel     = 'MEG';
cfg.parameter = 'powspctrm';
cfg.method      = 'analytic';
cfg.statistic   = 'ft_statfun_depsamplesT';
cfg.alpha       = 0.05;
cfg.correctm    = 'no';
%cfg.freq = [1 2];

Nsub = numel(subject_id);

cfg.design(1,1:2*Nsub)  = [ones(1,Nsub) 2*ones(1,Nsub)];
cfg.design(2,1:2*Nsub)  = [1:Nsub 1:Nsub];
cfg.ivar                = 1; % the 1st row in cfg.design contains the independent variable
cfg.uvar                = 2; % the 2nd row in cfg.design contains the subject number

% statistics
stat_vis_attend = ft_freqstatistics(cfg,itc_vis{:},itc_not_attend{:});

%%

cfg = [];
cfg.colormap = 'jet';
cfg.parameter = 'stat';
%cfg.maskparameter = 'mask';

% lower limit should be the upper limit of the critval
%cfg.zlim = [2.0796 7];
cfg.layout = 'neuromag306mag.lay';
ft_topoplotER(cfg, stat_vis_attend);


%% combine and run statistics
for  i = 1:numel(subject_id)
cfg = [];
itc_not_attend_cmb{i} = ft_combineplanar(cfg, itc_not_attend{i});
end 

% computing average for gradio
for i = 1:numel(subject_id)
tmp{i} = (itc_not_attend_cmb{i}.powspctrm./2);
itc_not_attend_cmb{i}.powspctrm = tmp{i};
end

clear tmp 

for  i = 1:numel(subject_id)
cfg = [];
itc_attend_cmb{i} = ft_combineplanar(cfg, itc_vis{i});
end 


for i = 1:numel(subject_id)
tmp{i} = (itc_attend_cmb{i}.powspctrm./2);
itc_attend_cmb{i}.powspctrm = tmp{i};
end


%%
cfg = [];
cfg.channel     = 'MEG';
cfg.parameter = 'powspctrm';
cfg.method      = 'analytic';
cfg.statistic   = 'ft_statfun_depsamplesT';
cfg.alpha       = 0.05;
cfg.correctm    = 'no';
%cfg.freq = [1 2];

Nsub = numel(subject_id);

cfg.design(1,1:2*Nsub)  = [ones(1,Nsub) 2*ones(1,Nsub)];
cfg.design(2,1:2*Nsub)  = [1:Nsub 1:Nsub];
cfg.ivar                = 1; % the 1st row in cfg.design contains the independent variable
cfg.uvar                = 2; % the 2nd row in cfg.design contains the subject number

% statistics
stat_vis_attend_cmb = ft_freqstatistics(cfg,itc_attend_cmb{:},itc_not_attend_cmb{:});

%% montecarlo

cfg = [];
cfg.channel     = 'MEGGRAD';
cfg.parameter = 'powspctrm';
cfg.method      = 'montecarlo';
cfg.statistic   = 'ft_statfun_depsamplesT';
cfg.alpha       = 0.025;
cfg.correctm         = 'no';
% cfg.freq = [6 16];
cfg.correctm         = 'cluster';
cfg.clusteralpha     = 0.05;
cfg.clusterstatistic = 'maxsum';
cfg.tail             = 0;
cfg.clustertail      = 0;
%cfg.alpha            = 0.025;
cfg.numrandomization = 10000;
%cfg.frequency = 1.6;
%cfg.latency = [-0.2 0.0];

%prepare neighbours
cfg_neigh=[];
% elec = [];
% elec.elecpos =template_grid.pos(template_grid.inside,:);
% elec.label   = hits{1}.label;
% elec.unit ='cm';
cfg_neigh.method = 'template';

%cfg_neigh.elec = elec;
%cfg_neigh.neighbourdist = 1.6;
cfg_neigh.layout = 'neuromag306cmb.lay';
cfg_neigh.grad = itc_vis{1}.grad; 
neigh = ft_prepare_neighbours(cfg_neigh);
cfg.neighbours       = neigh;

Nsub = numel(subject_id);

cfg.design(1,1:2*Nsub)  = [ones(1,Nsub) 2*ones(1,Nsub)];
cfg.design(2,1:2*Nsub)  = [1:Nsub 1:Nsub];
cfg.ivar                = 1; % the 1st row in cfg.design contains the independent variable
cfg.uvar                = 2; % the 2nd row in cfg.design contains the subject number

% statistics
stat_vis_attend_cmb = ft_freqstatistics(cfg,itc_attend_cmb{:},itc_not_attend_cmb{:});
%%
cfg = [];
cfg.colormap = 'jet';
cfg.parameter = 'stat';
%cfg.maskparameter = 'posclusterslabelmat';

% lower limit should be the upper limit of the critval
cfg.zlim = [-5 5];
cfg.layout = 'neuromag306cmb.lay';
ft_topoplotER(cfg, stat_vis_attend_cmb);
