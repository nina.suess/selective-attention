
clear all global
close all

%% init obob_ownft
addpath('C:/Users/Nina/Documents/MATLAB/obob_ownft');

cfg = [];
obob_init_ft(cfg);

%% load

%where are the files
data_folder = 'C:\Users\Nina\Documents\MATLAB\masterthesis_data';
in_folder = fullfile(data_folder, 'itc_not_attend');
%% loading files
% aud block, visual trigger

mod = 'aud';

subject_id = {
'19870319WLGL';...
};

for i = 1:numel(subject_id)
clean_data{i} = fullfile(in_folder, subject_id{i});
end

%% loading 

for i = 1:numel(subject_id)
data_itc{i} = fullfile(clean_data{i}, sprintf('%s_itc_%s_vis_trigger.mat', char(subject_id(i)), char(mod)));
end
%% loading data

for i = 1:numel(subject_id)
data_tmp{i} = load(data_itc{i});
data_tmp{i}.itc_vis_not_attend = data_tmp{i}.itc_aud_not_attend;
end 

data_tmp{1} = rmfield(data_tmp{1}, 'itc_aud_not_attend');
%% looking for empty cells and removing them
for i = 1:numel(subject_id)
    
    for ii = 1:2160
        nan_idx{ii} = find(isnan(data_tmp{i}.itc_vis_not_attend.fourierspctrm(ii, :, :)));
    end
    
    empty{i} = find(~cellfun(@isempty, nan_idx));
end
%% computing new itc 

for i = 1:numel(subject_id)

data_tmp{i}.F(empty{i}, :, :) = [];
data_tmp{i}.itc_vis_not_attend.fourierspctrm(empty{i}, :, :) = [];
data_tmp{i}.itc_vis_not_attend.cumsumcnt(empty{i}) = [];
data_tmp{i}.itc_vis_not_attend.cumtapcnt(empty{i}) = [];
data_tmp{i}.itc_vis_not_attend.trialinfo(empty{i}) = [];

data_tmp{i}.N = size(data_tmp{i}.F,1);
data_tmp{i}.itc.powspctrm      = data_tmp{i}.F./abs(data_tmp{i}.F);         % divide by amplitude  
data_tmp{i}.itc.powspctrm      = sum(data_tmp{i}.itc.powspctrm,1);   % sum angles - phsenwinkel im F
data_tmp{i}.itc.powspctrm      = abs(data_tmp{i}.itc.powspctrm)/data_tmp{i}.N ;   % take the absolute value and normalize
data_tmp{i}.itc.powspctrm      = squeeze(data_tmp{i}.itc.powspctrm); % remove the first singleton dimension

end

% removing empty cells also from fourier and save all?


%% saving new

for i = 1:numel(subject_id)
F = data_tmp{i}.F;
N = data_tmp{i}.N;
itc = data_tmp{i}.itc;
itc_vis_not_attend = data_tmp{i}.itc_vis_not_attend;

out_fname = fullfile(data_folder, sprintf('%s_itc_%s_vis_trigger_new.mat', char(subject_id{i}), char(mod)));

save(out_fname, '-v7.3', 'F', 'N', 'itc', 'itc_vis_not_attend');
end
%% save 
