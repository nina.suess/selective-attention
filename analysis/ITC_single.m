%% ITC 
 %clear all
 
clear all global
close all

%% init obob_ownft
addpath('C:/Users/Nina/Documents/MATLAB/obob_ownft');

cfg = [];
obob_init_ft(cfg);

%% load

%where are the preprocessing files
data_folder = 'C:\Users\Nina\Nextcloud\Masterthesis_Nina\analysis\itc_vis';

subject_id ='19961118BRSH';

mod = 'vis';

itc_data = fullfile(data_folder, subject_id, sprintf('%s_itc_%s.mat', subject_id, mod));

load(itc_data);


%% removing targets 
idx_targ = find(itc_audio.trialinfo == 2);

% in that case 190 is the number of targets
idx = reshape(idx_targ, [1, 190]);
F(idx, :, :) = [];
% itc_audio.fourierspctrm(idx, :, :) = [];
% itc_audio.trialinfo(idx) = [];
% itc_audio.cumsumcnt(idx) = [];
% itc_audio.cumtapcnt(idx) = [];


%% compute new itc without the targets

itc.powspctrm      = F./abs(F);         % divide by amplitude  
itc.powspctrm      = sum(itc.powspctrm,1);   % sum angles
itc.powspctrm      = abs(itc.powspctrm)/N;   % take the absolute value and normalize
itc.powspctrm      = squeeze(itc.powspctrm); % remove the first singleton dimension

%%
% die anzahl der trials - sind zu viel? kann man angle_osc gruppieren? 

% angle_osc hier berechnen - nur f�r magnetometer
cfg = [];
cfg.channel = 'megmag';
itc_mag = ft_selectdata(cfg, itc_audio);

foi = itc_audio.fourierspctrm(:, :, 3);
angle_osc = angle(foi);
histogram(angle_osc);

%% combine planar 

itc.grad=itc_vis.grad;
itc_cmb =ft_combineplanar([], itc);

%%

%denkt, es sind powerdaten und addiert auf über beide richtungen (deswegen
%powspctrm -summary (bei cmb)

cfg = [];
%cfg.parameter = 'powspctrm';
cfg.layout = 'neuromag306mag.lay';
ft_multiplotER(cfg, itc);

%% plot f�r cmb

tmp = (itc_cmb.powspctrm./2);
itc_cmb.powspctrm = tmp;

cfg = [];
%cfg.parameter = 'powspctrm';
cfg.layout = 'neuromag306mag.lay';
ft_multiplotER(cfg, itc_cmb);
