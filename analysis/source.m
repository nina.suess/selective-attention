%% quick source



%% init part
addpath /Users/gianpaolo/git/obob_ownft/
cfg = [];
cfg.package.svs = 'true';
cfg.package.obsolete = 'true';
obob_init_ft(cfg);



% head stuff, only the first time
cfg = [];
cfg.inputdat     = 'NW_block01_random.fif';
grad=ft_read_sens(cfg.inputdat);
dataall.grad = grad;



cfg = [];
cfg.mrifile = [];
cfg.headshape = 'NW_block01_random.fif';
[mriF, hshape, hdmF, segmentedmriF]=obob_coregister(cfg);
hdmF=ft_convert_units(hdmF, 'cm');
mriF=ft_convert_units(mriF, 'cm');



save NWhead.mat mriF* hdm* grad* hshape



%%
load mni_grid_1_5_cm_889pnts.mat %mni_grid_1_cm_2982pnts.mat



%%
cfg = [];
cfg.coordsys='neuromag';
cfg.grid.warpmni   = 'yes';
cfg.grid.template  = template_grid;
cfg.grid.nonlinear = 'yes'; % use non-linear normalization
cfg.mri            = mriF;
individual_grid               = ft_prepare_sourcemodel(cfg);



%%
cfg=[];
cfg.channel = 'MEG';
cfg.vol=hdmF;
cfg.grid=individual_grid;
cfg.grad=dataall.grad;
cfg.normalize='yes';
%cfg.inwardshift = -5;
%cfg.moveinward = -2.5;
lf=ft_prepare_leadfield(cfg,dataall);



%%
omission_random.grad = grad;



%% virtual sensors
cfg=[];
cfg.channel = {'MEG'};
cfg.hpfilter='yes';
cfg.hpfreq=1;
cfg.lpfilter='yes';
cfg.lpfreq=95;
cfg.hdm=hdmF;
cfg.grid=lf;



vsomission_random=obob_svs_beamtrials_lcmv(cfg, omission_random);





%% ERFs vs



verfomrandom  = ft_timelockanalysis([],vsomission_random);





%% bl



cfg = [];
cfg.baseline     = [-0.05 0] ;
cfg.baselinetype = 'relchange';
cfg.keeppolarity = false;
omrbl = obob_svs_timelockbaseline(cfg,verfomrandom);



%%
figure;
plot(omrbl.time, mean(omrbl.avg))
hold;
plot(omrbl.time, mean(omobl.avg),'r')



%% fft



cfg=[];
cfg.channel =  'all';
cfg.method='mtmfft';
cfg.taper='hanning';
cfg.foi=[1:0.5:100];
cfg.output='pow';
%cfg.padding = 'yes';
%cfg.keeptrials='yes';





vsensfomission_random=ft_freqanalysis(cfg, vsomission_random);







figure; hold all;
plot(vsensfomission_random.freq, log10(mean(vsensfomission_random.powspctrm)),'LineWidth',3)
legend('omission random','omission m-', 'omission m+','omission ordered')
title('PSD of omission condition, virtual sensors, different entropy levels , MAGS')





%% plot source
cfg=[];
cfg.sourcegrid =  individual_grid; %template_grid
cfg.parameter={'powspctrm'};
cfg.foilim=[8 14];
cfg.mri = mriF;



srcomission_random = obob_svs_virtualsens2source(cfg, vsensfomission_random);





%% %% plot source ERFs



cfg=[];
cfg.sourcegrid =  individual_grid; %template_grid
cfg.parameter={'avg'};
cfg.toilim=[0.05 0.15];
cfg.mri = mriF;



verf_random = obob_svs_virtualsens2source(cfg, omrbl);





%% single



cnt = verf_random;
cnt.pow = (verf_random.pow - verf_ordered.pow)



cfg = [];
cfg.funparameter = 'pow';
%cfg.maskparameter = 'mask2';
%cfg.atlas = '/Users/gianpaolo/git/fieldtrip/template/atlas/aal/ROI_MNI_V4.nii';
%cfg.roi = {'Calcarine_L', 'Calcarine_R'}
%cfg.interactive = 'no';
%cfg.crosshair = 'no';%'yes';
%cfg.axis = 'off';
%cfg.colorbar = 'yes';
cfg.funcolormap = 'jet';
%cfg.funcolorlim   = [0 1].*10^27%'zeromax' ;%'maxabs'%[-1 1].*10^22;
%cfg.funcolorlim   = [0 .6];%
%cfg.location      = [173 46 138];
%cfg.locationcoordinates = 'voxel';
ft_sourceplot(cfg, cnt);

